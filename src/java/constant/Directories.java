/*
 * 定数定義クラス
 * ＜ディレクトリ＞
 */
package constant;

public class Directories {

     /** ルートディレクトリ */
    public static final String DIR_ROOT
            = (System.getProperty("os.name").toLowerCase().indexOf("windows")==-1?"":"D:")
            +"/Tengagym/webapps/";
   
    /** ファイルアップロードディレクトリ */
    public static final String DIR_UPLOAD
            = (System.getProperty("os.name").toLowerCase().indexOf("windows")==-1?"":"D:")
            +"/Tengagym/webapps/img/";
    
}
