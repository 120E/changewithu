/*
 * 定数定義クラス
 * ＜値＞
 */
package constant;

public class Values {

    /** データソース名 */
    public static final String DATA_SOURCE = "change_with_u";

    /** COOKIEセッション名 */
    public static final String COOKIE_SESSION = "CHANGEWITHU";

    /** パスワードマスク（管理者用） */
    public static final String PASSWD_MASK_ADMIN = "change-with-u.jp";

    /** セッション時間 */
    public static final int SESSION_TIMOUTE = 60*60;//秒数

    
    /** stepパラメータ（初期画面） */
    public static final int STEP_INPUT = 0;
    /** stepパラメータ（確認） */
    public static final int STEP_CONFIRM = 1;
    /** stepパラメータ（反映） */
    public static final int STEP_AFFECT = 2;
    /** stepパラメータ（初期画面：要素編集等） */
    public static final int STEP_INPUT_CONTINUE = 3;
    /** stepパラメータ（初期画面：編集画面に戻る等） */
    //public static final int STEP_INPUT_BACK = 4;
    
}
