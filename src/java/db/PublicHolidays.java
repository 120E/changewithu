/*
* テーブルデータ
* ＜国民の休日＞
*/
package db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import util.StringEscapeUtils;
import util.StringUtils;

public class PublicHolidays implements Serializable {
    
    protected String date;
    protected boolean is_close_day;
    protected String comment;
    

    
    public String getDate(){ return date; }
    public boolean isCloseDay(){ return is_close_day; }
    public String getComment(){ return comment; }
    
    
    public void setDate(String value){ date = value; }
    public void setIsCloseDay(boolean value){ is_close_day = value; }
    public void setComment(String value){ comment = value; }
    

    /**
     * コンストラクタ（変数初期化）
     */
    public PublicHolidays () {
        super();
        date = "";
        is_close_day = true;
        comment = "";
        
    }
    
    public void set (db.PublicHolidays _obj){
        date = _obj.getDate();
        is_close_day = _obj.isCloseDay();
        comment = _obj.getComment();

    }
    
    
    /**
     * データ取得（ResultSetからの取り出し）
     * @param rs
     * @param list
     * @throws SQLException 
     */
    private static void getListFromResultSet (ResultSet rs, ArrayList<PublicHolidays> list) throws SQLException {
        while (rs.next ()) {
            PublicHolidays _obj = new PublicHolidays();
            _obj.setDate(rs.getString("date"));
            _obj.setIsCloseDay(rs.getInt("is_close_day")==1);
            _obj.setComment(rs.getString("comment").replaceAll("''", "'"));

            list.add(_obj);
        }
        rs.close ();
    }


    /**
     * @param condition " where "を含むこと
     * @param conn
     * @param _condition
     * @return 
     */
    public static ArrayList<PublicHolidays> makeList (Connection conn, String _condition) {
        ArrayList<PublicHolidays> list = new ArrayList<>();
        try (PreparedStatement pstm = conn.prepareStatement(
                "select date_format(`date`,'%Y/%m/%d') as `date`, `is_close_day`, `comment` from `public_holidays`"
                        + _condition
                        +" order by `date`"
            )) {
                getListFromResultSet(pstm.executeQuery(), list);
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return list;
    }

    
    
}
