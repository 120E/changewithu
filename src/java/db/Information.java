/*
* テーブルデータ
* ＜おしらせ＞
*/
package db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import util.StringEscapeUtils;
import util.StringUtils;

public class Information implements Serializable {
    
    protected int id;
    protected String title;
    protected String disp_date;
    protected String content;    
    protected int state;
    protected int deleted;
    protected int is_new;

    
    public int getId(){ return id; }
    public String getTitle(){ return title; }
    public String getDispDate(){ return disp_date; }
    public String getContent(){ return content; }
    public int getState(){ return state; }
    public int getDeleted(){ return deleted; }
    public boolean isNew() { return is_new==1; }
    
    
    public void setId(int value){ id = value; }
    public void setTitle(String value){ title = value; }
    public void setDispDate(String value){ disp_date = value; }
    public void setContent(String value){ content = value; }
    public void setState(int value){ state = value; }
    public void setDeleted(int value){ deleted = value; }
    public void setIsNew(int value){ is_new = value; }
    

    /**
     * コンストラクタ（変数初期化）
     */
    public Information () {
        super();
        id = 0;
        title = "";
        disp_date = "";
        content = "";
        state = 0;
        deleted = 0;
        is_new = 0;
    }
    
    public void set (db.Information _obj){
        id = _obj.getId();
        title = _obj.getTitle();
        disp_date = _obj.getDispDate();
        content = _obj.getContent();
        state = _obj.getState();
        deleted = _obj.getDeleted();
        is_new = _obj.isNew()?1:0;
    }
    
    
    /**
     * データ取得（ResultSetからの取り出し）
     * @param rs
     * @param list
     * @throws SQLException 
     */
    private static void getListFromResultSet (ResultSet rs, ArrayList<Information> list) throws SQLException {
        while (rs.next ()) {
            Information _obj = new Information();
            _obj.setId(rs.getInt("id"));
            _obj.setTitle(rs.getString("title").replaceAll("''", "'"));
            _obj.setDispDate(rs.getString("disp_date").replaceAll("''", "'"));
            _obj.setContent(rs.getString("content")==null? "" : rs.getString("content").replaceAll("''", "'"));
            _obj.setState(rs.getInt("state"));
            _obj.setDeleted(rs.getInt("deleted"));
            _obj.setIsNew(rs.getInt("is_new"));

            list.add(_obj);
        }
        rs.close ();
    }

    /**
     * データを削除
     * 注）passwdはencodeして格納
     * @param conn
     * @param _obj
     * @return 成否
     */
    public static boolean deleteData(Connection conn, Information _obj){
        int affectedCount = 0;
        try (PreparedStatement pstm = conn.prepareStatement("update `information`"
                            + " set `deleted`=1"
                            + " where `id`=?"
            )) {
                pstm.setInt(1, _obj.getId());
                affectedCount = pstm.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return affectedCount==1;
    }
    
    
    /**
     * データの状態を変更
     * 注）passwdはencodeして格納
     * @param conn
     * @param _obj
     * @return 成否
     */
    public static boolean stateData(Connection conn, Information _obj){
        int affectedCount = 0;
        int next_state = 0;
        int now_state = _obj.getState(); 
        if(now_state==0)
            next_state=1;
        else if(now_state==1)
            next_state=0;  
        
        try (PreparedStatement pstm = conn.prepareStatement("update `information`"
                            + " set `state`=?"
                            + " where `id`=?"
            )) {
                pstm.setInt(1, next_state);
                pstm.setInt(2, _obj.getId());
                affectedCount = pstm.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return affectedCount==1;
    }

    
    /**
     * データを変更
     * 注）passwdはencodeして格納
     * @param conn
     * @param _obj
     * @return 成否
     */
    public static boolean updateData(Connection conn, Information _obj){
        int affectedCount = 0;
        try (PreparedStatement pstm = conn.prepareStatement("update `information`"
                            + " set `title`=?"
                            + ", `disp_date`=?"
                            + ", `content`=?"
                            + ", `state`=?"
                            + ", `deleted`=0"
                            + " where `id`=?"
            )) {
                int pIndex = 1;
                pstm.setString(pIndex++, _obj.getTitle());//----------------PreparedStatementの場合はStringEscapeUtils.escapeSql()不要
                pstm.setString(pIndex++, _obj.getDispDate());
                pstm.setString(pIndex++, _obj.getContent());
                pstm.setInt(pIndex++, _obj.getState());
                pstm.setInt(pIndex++, _obj.getId());
                affectedCount = pstm.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return affectedCount==1;
    }

    
    
    /**
     * データを追加
     * 注）passwdはencodeして格納
     * @param conn
     * @param _obj
     * @return 成否
     */
    public static boolean insertData(Connection conn, Information _obj){
        int affectedCount = 0;
        try (PreparedStatement pstm = conn.prepareStatement(
                "insert into `information` values"
                + " ("
                        + "0,"//id
                        + "?,"//title
                        + "?,"//disp_date
                        + "?,"//content
                        + "?,"//state
                        + "0)"//deleted
            )) {
                int pIndex = 1;
                pstm.setString(pIndex++, _obj.getTitle());//----------------PreparedStatementの場合はStringEscapeUtils.escapeSql()不要
                pstm.setString(pIndex++, _obj.getDispDate());
                pstm.setString(pIndex++, _obj.getContent());
                pstm.setInt(pIndex++, _obj.getState());
                affectedCount = pstm.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return affectedCount==1;
    }

    
    /**
     * データ取得(CMS用)
     * 注）deletedは常に対象外
     * @param conn
     * @param _id
     * @param _genre
     * @return list 
     */
    public static ArrayList<Information> makeList (Connection conn, int _id) {        
        ArrayList<Information> list = new ArrayList<>();
        try (PreparedStatement pstm = conn.prepareStatement(
                "select *, now()<date_add(disp_date, interval 7 day) as is_new from `information`"
                        + " where `deleted`=0"
                        + " and `state`=1"
                        + (_id==0? "" : " and `id`=?")
                        + " order by `disp_date` desc, `id` desc"
            )) {
                int pstmIndex = 1;
                if (_id != 0)
                    pstm.setInt(pstmIndex++, _id);
                getListFromResultSet(pstm.executeQuery(), list);
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return list;
    }
    public static ArrayList<Information> makeListCMS (Connection conn, int _id) {        
        ArrayList<Information> list = new ArrayList<>();
        try (PreparedStatement pstm = conn.prepareStatement(
                "select *, now()<date_add(disp_date, interval 7 day) as is_new from `information`"
                        + " where `deleted`=0"
                        + (_id==0? "" : " and `id`=?")
                        + " order by `disp_date` desc, `id` desc"
            )) {
                int pstmIndex = 1;
                if (_id != 0)
                    pstm.setInt(pstmIndex++, _id);
                getListFromResultSet(pstm.executeQuery(), list);
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return list;
    }
    
    
    
    
}
