/*
* テーブルデータ
* ＜営業カレンダー＞
*/
package db;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import util.StringEscapeUtils;
import util.StringUtils;

public class ServiceCalendar implements Serializable {
    
    protected String date;
    protected boolean is_close_day;
    protected int hour_start = 15;
    protected int hour_end = 22;
    protected String comment;
    

    
    public String getDate(){ return date; }
    public boolean isCloseDay(){ return is_close_day; }
    public int getHourStart(){ return hour_start; }
    public int getHourEnd(){ return hour_end; }
    public String getComment(){ return comment; }
    
    
    public void setDate(String value){ date = value; }
    public void setIsCloseDay(boolean value){ is_close_day = value; }
    public void setHourStart(int value){ hour_start = value; }
    public void setHourEnd(int value){ hour_end = value; }
    public void setComment(String value){ comment = value; }
    

    /**
     * コンストラクタ（変数初期化）
     */
    public ServiceCalendar () {
        super();
        date = "";
        is_close_day = false;
        hour_start = 15;
        hour_end = 22;
        comment = "";
        
    }
    
    public void set (db.ServiceCalendar _obj){
        date = _obj.getDate();
        is_close_day = _obj.isCloseDay();
        hour_start = _obj.getHourStart();
        hour_end = _obj.getHourEnd();
        comment = _obj.getComment();

    }
    
    
    
    
    public static boolean updateData(Connection conn, ServiceCalendar _obj){
        int affectedCount = 0;
        try (PreparedStatement pstm = conn.prepareStatement(
                "replace into `service_calendar` values("
                            + "?" //date
                            + ", ?" //is_close_day
                            + ", ?" //hour_start
                            + ", ?" //hour_end
                            + ", ?" //comment
                            + ")"
            )) {
                int pIndex = 1;
                pstm.setString(pIndex++, _obj.getDate().replace("/", "-"));
                pstm.setInt(pIndex++, _obj.isCloseDay()?1:0);
                pstm.setInt(pIndex++, _obj.getHourStart());
                pstm.setInt(pIndex++, _obj.getHourEnd());
                pstm.setString(pIndex++, _obj.getComment());
                affectedCount = pstm.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return affectedCount==1 || affectedCount==2;
    }

    
    
    
    
    /**
     * データ取得（ResultSetからの取り出し）
     * @param rs
     * @param list
     * @throws SQLException 
     */
    private static void getListFromResultSet (ResultSet rs, ArrayList<ServiceCalendar> list) throws SQLException {
        while (rs.next ()) {
            ServiceCalendar _obj = new ServiceCalendar();
            _obj.setDate(rs.getString("date"));
            _obj.setIsCloseDay(rs.getInt("is_close_day")==1);
            _obj.setHourStart(rs.getInt("hour_start"));
            _obj.setHourEnd(rs.getInt("hour_end"));
            _obj.setComment(rs.getString("comment").replaceAll("''", "'"));

            list.add(_obj);
        }
        rs.close ();
    }


    /**
     * @param condition " where "を含むこと
     * @param conn
     * @param _condition
     * @return 
     */
    public static ArrayList<ServiceCalendar> makeList (Connection conn, String _condition) {
        ArrayList<ServiceCalendar> list = new ArrayList<>();
        try (PreparedStatement pstm = conn.prepareStatement(
                "select"
                        + " date_format(`date`,'%Y/%m/%d') as `date`"
                        + ", `is_close_day`"
                        + ", `hour_start`"
                        + ", `hour_end`"
                        + ", `comment`"
                        + " from `service_calendar`"
                        +" "+_condition
                        +" order by `date`"
            )) {
                getListFromResultSet(pstm.executeQuery(), list);
        } catch (SQLException se) {
            se.printStackTrace (System.out);
        }
        return list;
    }

    
    
}
