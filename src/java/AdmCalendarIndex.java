import constant.Values;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.NumberUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import util.ArrayUtils;
import util.Database;
import util.JspUtils;

public class AdmCalendarIndex extends HttpServlet {

    @Override
    public void init(javax.servlet.ServletConfig config) throws ServletException {
        super.init(config);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response, boolean isPostMethod) throws ServletException, IOException {
        Connection conn = Database.getConnectionFromDataSource(Values.DATA_SOURCE);

        try{
            //取得パラメータを分解
            util.HttpServletRequestEmulator hsre = new util.HttpServletRequestEmulator(request);
            
            Calendar _cal = Calendar.getInstance();
            int _currentYear = _cal.get(Calendar.YEAR);
            int _currentMonth = _cal.get(Calendar.MONTH)+1;
            _cal.set(Calendar.DAY_OF_MONTH, 1);
            _cal.set(Calendar.MONTH, _cal.get(Calendar.MONTH)+1);
            int _nextYear = _cal.get(Calendar.YEAR);
            int _nextMonth = _cal.get(Calendar.MONTH)+1;
            //System.out.println("_currentMonth="+_currentYear+"/"+_currentMonth);
            //System.out.println("_nextMonth="+_nextYear+"/"+_nextMonth);
            request.setAttribute("dbServiceCalendarCurrent", db.ServiceCalendar.makeList(conn, "where year(`date`)="+_currentYear+" and month(`date`)="+_currentMonth));
            request.setAttribute("dbServiceCalendarNext", db.ServiceCalendar.makeList(conn, "where year(`date`)="+_nextYear+" and month(`date`)="+_nextMonth));
            request.setAttribute("dbPublicHolidaysCurrent", db.PublicHolidays.makeList(conn, "where year(`date`)="+_currentYear+" and month(`date`)="+_currentMonth));
            request.setAttribute("dbPublicHolidaysNext", db.PublicHolidays.makeList(conn, "where year(`date`)="+_nextYear+" and month(`date`)="+_nextMonth));
            
            util.JspUtils.sendToJsp (request, response, "/adm/CalendarIndex.jsp");
            
        
        }catch(Exception ex){
            System.out.println("exception occurs in "+getClass().getName()+".");
            ex.printStackTrace(System.out);
            //response.sendRedirect("/");
        }finally{
            Database.disConnect(conn);
        }
    }
    
    
    /**
     * 内閣府のサイト
     * https://www8.cao.go.jp/chosei/shukujitsu/gaiyou.html
     * からcsvを取得して格納
     */
    private boolean getPublicHolidaysCSV(Connection conn){
        boolean _result = false;
        HttpClient httpclient = new HttpClient();
        httpclient.getParams().setParameter(HttpMethodParams.USER_AGENT, " Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");
        HttpMethod httpget = new GetMethod("https://www8.cao.go.jp/chosei/shukujitsu/syukujitsu.csv");
        try {
            int statusCode = httpclient.executeMethod(httpget);  
            BufferedReader br = new BufferedReader(new InputStreamReader(httpget.getResponseBodyAsStream(), "Windows-31J"));
            String _line = "";
            PreparedStatement pstm = conn.prepareStatement("replace into public_holidays values(?,1,?)");
            boolean _isFirstLine = true;
            while((_line=br.readLine())!=null){
                //1行目は無視
                if(_isFirstLine){
                    _isFirstLine = false;
                    continue;
                }
                //_csv += new String(_line.getBytes(),"UTF-8")+"\n";
                //_csv += _line +"\n";
                String[] _part = _line.split(",");
                if(_part.length==2){
                    pstm.setString(1, _part[0]);
                    pstm.setString(2, _part[1]);
                    pstm.executeUpdate();
                }
            }
            br.close();
            pstm.close();
            //System.out.println(_csv);
            
            _result = true;
        }catch(Exception e){
            e.printStackTrace(System.out);
        } finally {  
            httpget.releaseConnection();  
        }
        return _result;
    }
    

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, false);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response, true);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
