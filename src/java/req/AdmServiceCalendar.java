/*
 * POSTデータの取得クラス
 * テーブルデータと粗共通なので継承して利用
 */
package req;

import constant.Directories;
import constant.Values;
import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import util.ArrayUtils;
import util.HttpServletRequestEmulator;
import util.NumberUtils;
import util.StringUtils;

public class AdmServiceCalendar extends db.ServiceCalendar {

    //
    // ここは共通
    //
    /** 受信データフォルダ */
    public static String DIR = Directories.DIR_UPLOAD;
    /** 受信データ */
    private javax.servlet.http.HttpServletRequest request;
    /** 受信データ */
    private util.HttpServletRequestEmulator hsre;
    /** 入力チェック結果 */
    private boolean [] validation;
    /** エラー内容 */
    private String [] errorMessage;
    /** 操作種別（追加／変更／削除等） */
    private String action;
    /** 操作ステップ（入力／確認／登録等） */
    private int step;
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
    Connection conn;
    
    String loginPw2 = "";

    /**
     * ゲッター
     * @return 各データ 
     */
    public HttpServletRequest getHttpServletRequest () {return request;}
    public HttpServletRequestEmulator getHttpServletRequestEmulator () {return hsre;}
    public boolean [] getValidation () {return validation;}
    public String [] getErrorMessage () {return errorMessage;}
    public String getAction () {return action;}
    public int getStep () {return step;}
    
    /**
     * セッター
     * @param value 各データ
     */
    public void setAction (String value) {action = value;}
    public void setStep (int value) {step = value;}

    
    
    /**
     * URLパラメータ分解処理
     * @param request
     * @param conn 二重登録チェックのために使う（通常不要）
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException 
     */
    public void extract (javax.servlet.http.HttpServletRequest request, Connection conn) throws javax.servlet.ServletException, java.io.IOException {
        extract(request, conn, DIR);
    }
    /**
     * URLパラメータ分解処理
     * @param request
     * @param conn 二重登録チェックのために使う（通常不要）
     * @param uploadDir
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException 
     */
    public void extract (javax.servlet.http.HttpServletRequest request, Connection conn, String uploadDir) throws javax.servlet.ServletException, java.io.IOException {
        this.request = request;
        this.conn = conn;
        this.hsre = new util.HttpServletRequestEmulator (request, uploadDir, "utf-8");
        extract (hsre, conn);
    }
    /**
     * URLパラメータ分解処理
     * @param hsre
     * @param conn 二重登録チェックのために使う（通常不要）
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException 
     */
    public void extract (util.HttpServletRequestEmulator hsre, Connection conn) throws javax.servlet.ServletException, java.io.IOException {
        this.hsre = hsre;
        this.conn = conn;
       
        
        action = hsre.getParameter ("action");
        step = NumberUtils.toInt(hsre.getParameter ("step"), 0);
        date = hsre.getParameter ("date");
        is_close_day = hsre.getParameter ("is_close_day").equals("1");
        hour_start = NumberUtils.toInt (hsre.getParameter ("hour_start"), 0);
        hour_end = NumberUtils.toInt (hsre.getParameter ("hour_end"), 0);
        comment = hsre.getParameter ("comment");
        if(is_close_day){
            hour_start = 0;
            hour_end = 0;
        }

        //
        // 追加・編集操作の確認・完了画面への遷移時のみ入力チェック
        //
        validation = new boolean [1];
        Arrays.fill(validation, true);
        errorMessage = new String[validation.length];
        Arrays.fill(errorMessage, "");
        if(
            ArrayUtils.contains(new String[]{"add", "edit"}, action)
            && ArrayUtils.contains(new int[]{Values.STEP_CONFIRM, Values.STEP_AFFECT}, step)
            ){
            setValidation ();
            //エラーを含む場合は、入力画面に戻す（stepをゼロに）
            if(ArrayUtils.contains(validation, false)){
                step = Values.STEP_INPUT;
            }
        }
    }
    
    
    
    /**
     * 入力チェック
     */
    private boolean checkLength (String value, int from, int to) {
        //System.out.println (value.trim ().length ()+": "+value);
        return (from <= value.trim ().length ()) && (value.trim ().length () <= to);
    }
    private boolean checkValue (int value) {
        return (value!=0) ;
    }
    private void setValidation () {
        validation[0] = checkLength(date, 1, 255);
       
        errorMessage = new String[]{
                            validation[0]? "" : "<em>日付は必須入力です。</em>"
                            };
    }
    

}
