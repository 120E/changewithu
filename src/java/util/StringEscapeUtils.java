/*
 * StringEscapeUtils.java
 *
 * Created on 2004/10/28, 10:35
 */

package util;

/**
 *
 * @author  Kubo
 */
public class StringEscapeUtils extends org.apache.commons.lang.StringEscapeUtils {
    
    /** Creates a new instance of StringEscapeUtils */
    public StringEscapeUtils () {
    }
    
    public static String escapeHtml (String s) {
        if (s == null || s.length () == 0) {
            return "";
        }
        char sArray [] = s.toCharArray();
        String outString = "";
        for (int i = 0; i < sArray.length; i ++) {
            switch (sArray [i]) {
                case '<':  outString += "&lt;"; break;
                case '>':  outString += "&gt;"; break;
                case '&':  outString += "&amp;";    break;
                case '\"':  outString += "&quot;";  break;
                //case '\'':  outString += "&apos;";  break;
                default: outString += sArray [i];
            }
        }
        return outString;
    }
    
    public static String escapeHtmlPlusBr (String s) {
        return escapeHtml (s).replaceAll ("[\n]", "<br>");
    }

    /** クォートを変換
     * @param inString 変換文字列
     * @return 変換後文字列
     */
    public static String stripQuote(String inString) {
        if (inString == null) {
            return "";
        }
        if (inString.length() == 0) {
            return "";
        }
        char inStringArray[] = inString.toCharArray();
        String outString = "";
        for (int i = 0; i < inStringArray.length; i++) {
            switch(inStringArray[ i ]) {
                case '\'':  outString += "\\\'";    break;
                case '\"':  outString += "\\\"";    break;
                case '\\':  outString += "\\\\";    break;
                default: outString += inStringArray[ i ];
            }
        }
        return outString;
    }
}
