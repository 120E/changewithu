/*
 * ArrayUtils.java
 *
 * Created on February 28, 2006, 11:28 AM
 */

package util;

import java.io.Serializable;

/**
 * @author FFT corp.
 */
public class ArrayUtils extends org.apache.commons.lang.ArrayUtils {
    
    public ArrayUtils () {
    }
    

    
    /**
     * 配列に追加
     * ArrayUtils.add(null, 0)   = [0]
     * ArrayUtils.add([1], 0)    = [1, 0]
     * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
     * @param array the array to copy and add the element to, may be null
     * @param element the object to add at the last index of the new array 
     */
    public static int[] add (int[] array, int element) {
        int[] returnArray = null;
        if (array == null || array.length == 0) {
            returnArray = new int [1];
            returnArray[0] = element;
        } else {
            returnArray = new int [array.length + 1];
            for (int i = 0; i < array.length; i++) {
                returnArray[i] = array[i];
            }
            returnArray[array.length] = element;
        }
        return returnArray;
    }
    
    /**
     * 配列に追加
     * ArrayUtils.add(null, 0)   = [0]
     * ArrayUtils.add([1], 0)    = [1, 0]
     * ArrayUtils.add([1, 0], 1) = [1, 0, 1]
     * @param array the array to copy and add the element to, may be null
     * @param element the object to add at the last index of the new array 
     */
    public static String[] add (String[] array, String element) {
        String[] returnArray = null;
        if (array == null || array.length == 0) {
            returnArray = new String [1];
            returnArray[0] = element;
        } else {
            returnArray = new String [array.length + 1];
            for (int i = 0; i < array.length; i++) {
                returnArray[i] = array[i];
            }
            returnArray[array.length] = element;
        }
        return returnArray;
    }
    
    /**
     * 配列内の存在チェック
     * ArrayUtils.contains(null, 0)   = false
     * ArrayUtils.contains([1], 0)    = false;
     * ArrayUtils.contains([1, 0], 1) = true;
     * @param array the array to copy and add the element to, may be null
     * @param element the object to add at the last index of the new array 
     */
    public static boolean contains (int[] array, int element) {
        boolean result = false;
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                if(array[i]==element){
                    result=true;
                    break;
                }
            }
        }
        return result;
    }
    public static boolean contains (long[] array, long element) {
        boolean result = false;
        if (array != null && array.length > 0) {
            for (int i = 0; i < array.length; i++) {
                if(array[i]==element){
                    result=true;
                    break;
                }
            }
        }
        return result;
    }
    
    
}
