/*
 * FileManager.java
 *
 * Created on 2004/05/24, 20:40
 */

package util;

/**
 * ファイル操作ユーティリティ
 * @author  FFT corp.
 */
public class FileManager {
    
    /** Creates a new instance of FileManager */
    public FileManager() {
    }
    
    /**
     * ファイルのコピー
     * @param src コピー元ファイル名
     * @param dest コピー先ファイル名
     * @return true:正常 false:エラー
     */
    public static boolean copyFile(String src, String dest, boolean overwrite) {
        boolean result = true;
        try {
            java.io.File destFile = new java.io.File (dest);
            if(!(new java.io.File(src)).exists())
                return false;
            java.io.File destDir = destFile.getParentFile ();
            if (!destDir.exists ()) {
                destDir.mkdirs ();
            }
            if(destFile.exists()){
                if(overwrite)
                    destFile.delete();
                else{
                    System.out.println("DEST FILE exists.("+dest+")");
                    return false;
                }
            }
            java.io.FileInputStream in = new java.io.FileInputStream(src);
            java.io.FileOutputStream out = new java.io.FileOutputStream(dest);
            byte [] buffer = new byte [128*1024];
            int len;
            while ((len = in.read(buffer, 0, buffer.length))>=0) {
                out.write(buffer, 0, len);
            }
            out.flush();
            in.close();
            out.close();
        } catch (java.io.IOException ie) {
            result = false;
            System.err.println("File Copy Error:" + ie.getLocalizedMessage());
        }
        return result;
    }
    
    /** 
     * 指定したパスのディレクトリを作成する<br>
     * すでにディレクトリがある場合、同名のファイルがある場合は何もしない<br>
     * @param ディレクトリのパス
     * @return 結果（作成：true）
     */
    public static boolean makeDirectory(String path) {
        boolean result = true;
        java.io.File mkDir = new java.io.File(path);
        if (!mkDir.exists()) {
            mkDir.mkdir();
        } else {
            result = false;
        }
        return result;
    }
    
    /**
     * ディレクトリの削除
     * @param directory 削除するディレクトリ
     * @param deleteSubDirectory サブディレクトリを削除するか(true:削除 false:削除しない)<br>
     * ※deleteSubDirectoryがfalseの場合はdirectory内のファイルのみ削除
     */
    public static void deleteDirectory(String directory, boolean deleteSubDirectory) {
        String targetDir = directory.endsWith("/") ? directory : directory + "/";
        java.io.File delete = new java.io.File(targetDir);
        String fileList[] = null;
        if (delete.isDirectory()) {
            fileList = delete.list();
            for (int i = 0; i < fileList.length; i++) {
                java.io.File isDir = new java.io.File(targetDir + fileList[ i ]);
                if (isDir.isFile()) {
                    // ファイルを削除
                    isDir.delete();
                } else {
                    // 再起処理
                    if (deleteSubDirectory) {
                        deleteDirectory(targetDir + fileList[ i ], deleteSubDirectory);
                    }
                }
            }
            // 空ディレクトリを削除
            delete.delete();
        }
    }
    
    /**
     * InputStreamをファイルに保存
     * @param is 入力ストリーム
     * @param f 出力ファイル
     */
    public static void saveToFile (java.io.InputStream is, java.io.File f) {
        if (f.exists ())
            return;
        try {
            f.createNewFile ();
            java.io.FileOutputStream fos = new java.io.FileOutputStream (f);
            int readCount = 0;
            byte [] buffer = new byte [64*1024];
            while ((readCount = is.read (buffer, 0, buffer.length)) > 0) {
                fos.write (buffer, 0 , readCount);
            }
            fos.flush ();
            fos.close ();
        } catch (java.io.IOException ie) {
            System.err.println ("File save Error(" + f.getAbsolutePath () + "):" + ie.getLocalizedMessage ());
        }
    }
    
    
}
