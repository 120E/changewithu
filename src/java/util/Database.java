package util;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Database {
    
    
    /**
     * データソースからコネクションを取得
     * @return 
     */
    public static Connection getConnectionFromDataSource(String dataSource){
        Connection conn = null;
        try {
            InitialContext ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/"+dataSource);
            try {
                conn = ds.getConnection();
            } catch (SQLException ex) {
                ex.printStackTrace(System.err);
            }
        } catch (NamingException ex) {
                ex.printStackTrace(System.err);
        }
        return conn;
    }
    public static void disConnect (java.sql.Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (java.sql.SQLException se) {
            } finally {
                try {
                    conn.close();
                } catch (java.sql.SQLException se) {
                }
            }
        }
    }
    
    

}
