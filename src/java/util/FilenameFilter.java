/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;



/**
 * ファイル名フィルタ
 */
public class FilenameFilter implements java.io.FilenameFilter {
    String prefix,suffix;
    public FilenameFilter(String prefix, String suffix) {
        this.prefix = prefix;
        this.suffix = suffix;
    }
    public boolean accept(java.io.File dir, String filename) {
        return filename.startsWith(prefix)&&filename.endsWith(suffix);
    }
}