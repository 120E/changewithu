/*
 * NumberUtils.java
 *
 * Created on February 28, 2006, 11:48 AM
 */

package util;

import java.io.Serializable;

/**
 * @author FFT corp.
 */
public class NumberUtils extends org.apache.commons.lang.math.NumberUtils {
    
    public NumberUtils () {
    }
    
    
    
    /**
     * String を Long に変換
     * NumberUtils.toLong(null) = 0L
     * NumberUtils.toLong("")   = 0L
     * NumberUtils.toLong("1")  = 1L
     * @param str the string to convert, may be null 
     */
    public static long toLong(String str) {
        long returnLong = 0;
        if (str != null
        && str.length () > 0
        && util.StringUtils.isValidString(str, "0123456789")) {
            returnLong = Long.parseLong (str);
        }
        return returnLong;
    }
    
    
    
    /**
     * String を Long に変換<br>
     * 変換できない場合はdefaultValueを使用する
     * NumberUtils.toLong(null, 1L) = 1L
     * NumberUtils.toLong("", 1L)   = 1L
     * NumberUtils.toLong("1", 0L)  = 1L
     * @param str the string to convert, may be null 
     * @param defaultValue - the default value 
     */
    public static long toLong(String str, long defaultValue) {
        long returnLong = defaultValue;
        if (str != null
        && str.length () > 0
        && util.StringUtils.isValidString(str, "0123456789")) {
            returnLong = Long.parseLong (str);
        }
        return returnLong;
    }
    
    
    /**
     * 配列を連結（String）
     * to({1,2,3},",") = "1,2,3";
     * @param ary int array
     * @param delim delimiter
     * @return String
     */
    public static String join(int[] ary, String delim){
        String result="";
        if(ary!=null)
        for(int n=0; n<ary.length; n++)
            result += (result.length()==0?"":delim) + ary[n];
        return result;
    }
}
