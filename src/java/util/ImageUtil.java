package util;

import java.io.File;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ImageUtil
{
    public ImageUtil (){
    }


    /**
     * 画像ファイルのサイズを取得
     * @param imagePath ファイルパス（URLも可）
     * @return サイズ配列 int [2] ([0]=width,[1]=height)
     */
    public static int [] getImageSize (String imagePath) {
        int [] imageSize = {0, 0};
        try {
            javax.swing.ImageIcon imageIcon = null;
            if (imagePath.startsWith("http://")) {
                imageIcon = new javax.swing.ImageIcon (new java.net.URL (imagePath));
            } else {
                imageIcon = new javax.swing.ImageIcon (imagePath);
            }
            imageSize [0] = imageIcon.getIconWidth ();
            imageSize [1] = imageIcon.getIconHeight ();
            if (imageSize [0] == -1 || imageSize [1] == -1) {
                System.out.println ("ImageUtil.java:対象ファイルが見つかりません。 (" + imagePath + ")");
            }
        } catch (java.net.MalformedURLException mue) {
            System.err.println(mue.getLocalizedMessage());
//            mue.printStackTrace (System.out);
        }
        return imageSize;
    }

    /**
     * HTMLをパースし<img>から画像をコピー
     * @param html
     * @param absolutePathFrom
     * @param absolutePathTo
     * @return 
     */
    public static int copyImageInHtml(String html, String absolutePathFrom, String absolutePathTo, boolean overwrite){
        System.out.println(html);
        int affectedCount=0;
        Document doc = Jsoup.parse(html);
        Elements media = doc.select("[src]");
        for (Element _src : media) {
            if(!_src.tagName().equals("img"))
                continue;
            String _image = _src.attr("src");
            if(_image.length()==0)
                continue;
            String _absoluteImagePathFrom = 
                    (absolutePathFrom.endsWith("/")?absolutePathFrom.substring(0,absolutePathFrom.length()-1):absolutePathFrom)
                    +(_image.startsWith("/")?"":"/")
                    +_image;
            String _absoluteImagePathTo = 
                    (absolutePathTo.endsWith("/")?absolutePathTo.substring(0,absolutePathTo.length()-1):absolutePathTo)
                    +(_image.startsWith("/")?"":"/")
                    +_image;
            File _fileFrom = new File(_absoluteImagePathFrom);
            if(!_fileFrom.exists() || !_fileFrom.canRead()){
                System.out.println("cannot read image "+_absoluteImagePathFrom);
                continue;
            }
            if(FileManager.copyFile(_absoluteImagePathFrom, _absoluteImagePathTo, overwrite)){
                System.out.println(_image+" copy successful.");
                affectedCount++;
            }else
                System.out.println(_image+" copy failure.");
        }
//                println(" * %s: <%s> %sx%s (%s)",
//                        src.tagName(), src.attr("abs:src"), src.attr("width"), src.attr("height"),
//                        trim(src.attr("alt"), 20));
//            else
//                print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
//        }
        return affectedCount;
    }
    
    
    public static void main(String[] arg){
        String html = "<html>\n" +
            "    <head>\n" +
            "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n" +
            "            <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
            "        <title>コンテンツの管理</title>\n" +
            "        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/adm.css\"/>\n" +
            "    </head>\n" +
            "    \n" +
            "    <body>\n" +
            "        <div id=\"HEADER\">\n" +
            "            <div style=\"float:right;\">「 コンテンツの管理 」</div>\n" +
            "            <a href=\"./\" class=\"btnUpward\" title=\"トップメニューへ戻ります\">トップメニューへ戻る</a>\n" +
            "        </div>\n" +
            "        <div style=\"text-align:center;margin:100px 0;font-size:16px;\">\n" +
            "            <a href=\"page/\">ページの管理</a><br>\n" +
            "            <a href=\"drive/\">ドライブプランの管理</a><br>\n" +
            "            <a href=\"ticket/\">チケットの管理</a><br>\n" +
            "            <a href=\"tour/\">ツアーの管理</a><br>\n" +
            "            <a href=\"cep/\">CEPの管理</a><br>\n" +
            "	</div>\n" +
            "       <img src=\"img/tmp/1448357884191.png\" alit=\"tmp img src\">\n" +
            "    </body>\n" +
            "</html>";
        copyImageInHtml(html, "/cnexco/travel/webapps/", "/cnexco/hayatabi/webapps/", true);
    }
}

