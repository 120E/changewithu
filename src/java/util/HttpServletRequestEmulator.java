/*
 * HttpServletRequestEmulator.java
 *
 * Modified on 2004/11/01
 * (if getParameter(name) is null then returns "".)
 */

package util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

import util.multipart.Part;
import util.multipart.ParamPart;
import util.multipart.FilePart;
import util.multipart.MultipartParser;
import util.multipart.FileRenamePolicy;
import util.multipart.UploadedFile;

/** マルチパート対応サーブレットリクエストクラス
 * HttpServletRequestクラスインターフェースをエミュレートする
 * @author  FFT corp.
 */
public class HttpServletRequestEmulator {
    
    private HttpServletRequest request;

    /** 定数:標準アップロードサイズ(50M) */
    private static final int DEFAULT_MAX_POST_SIZE = 1024000 * 50;
    /** 定数:デフォルトエンコーディング */
    private static final String DEFAULT_ENCODING = "utf-8";
    /** 定数:標準アップロードディレクトリ名 */
    private static final String DEFAULT_SAVE_DIRECTORY = "req_upload";
    
    /** パラメータ文字列 */
    protected Hashtable params = null;
    /** アップロードファイル名 */
    protected Hashtable files = null;
    /** エンコーディング */
    private String strEncoding = DEFAULT_ENCODING;
    /** 標準アップロードパス、イニシャライザにより初期化*/
    protected static String defaultSavePath = null;

    /** イニシャライザ */
    static {
        //標準アップロードディレクトリの準備
        //defaultSavePath = makeTempDir(System.getProperty("java.io.tmpdir"), DEFAULT_SAVE_DIRECTORY);
        defaultSavePath = makeTempDir(System.getProperty("catalina.home"), DEFAULT_SAVE_DIRECTORY);
    }
    
    /** 引数無しの標準コンストラクタ */
    public HttpServletRequestEmulator() {
    }
    
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、かつ、アップロードサイズを1メガバイトに制限して、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request) throws IOException {
        this(request, defaultSavePath, DEFAULT_MAX_POST_SIZE, new util.multipart.DefaultFileRenamePolicy());
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、かつ、アップロードサイズを1メガバイトに制限して、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory) throws IOException {
        this(request, saveDirectory, DEFAULT_MAX_POST_SIZE);
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory, int maxPostSize) throws IOException {
        this(request, saveDirectory, maxPostSize, DEFAULT_ENCODING, new util.multipart.DefaultFileRenamePolicy());
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、かつ、アップロードサイズを1メガバイトに制限して、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   encoding        (String)エンコーディング
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory, String encoding) throws IOException {
        this(request, saveDirectory, DEFAULT_MAX_POST_SIZE, encoding, new util.multipart.DefaultFileRenamePolicy());
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   policy          (FileRenamePolicy)ファイル名重複防止クラス
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory, int maxPostSize, FileRenamePolicy policy) throws IOException {
        this(request, saveDirectory, maxPostSize, DEFAULT_ENCODING, policy);
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   encoding        (String)エンコーディング
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory, int maxPostSize, String encoding) throws IOException {
        this(request, saveDirectory, maxPostSize, encoding, new util.multipart.DefaultFileRenamePolicy());
    }
    /** HttpServletRequestEmulatorクラスコンストラクタ
     * アップロードされたファイルを保存し、
     * 指定されたリクエストを扱うために新しいクラスを構築します。
     * 内容が大きすぎる場合、IOExceptionが投げられます。コンストラクタはmultipart/form-dataを解析し、
     * 何らかの問題がある場合、IOExceptionを投げます。
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   encoding        (String)エンコーディング
     * @param   policy          (FileRenamePolicy)ファイル名重複防止クラス
     * @throws  IOException     (IOException)
     */
    public HttpServletRequestEmulator(HttpServletRequest request, String saveDirectory, int maxPostSize, String encoding, FileRenamePolicy policy) throws IOException {
        setRequest(request, saveDirectory, maxPostSize, encoding, policy);
    }
    
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request) throws IOException {
        setRequest(request, defaultSavePath, DEFAULT_MAX_POST_SIZE, new util.multipart.DefaultFileRenamePolicy());
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory) throws IOException {
        setRequest(request, saveDirectory, DEFAULT_MAX_POST_SIZE);
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory, int maxPostSize) throws IOException {
        setRequest(request, saveDirectory, maxPostSize, null, null);
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   encoding        (String)エンコーディング
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory, String encoding) throws IOException {
        setRequest(request, saveDirectory, DEFAULT_MAX_POST_SIZE, encoding, null);
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   policy          (FileRenamePolicy)ファイル名重複防止クラス
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory, int maxPostSize, FileRenamePolicy policy) throws IOException {
        setRequest(request, saveDirectory, maxPostSize, null, policy);
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   encoding        (String)エンコーディング
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory, int maxPostSize, String encoding) throws IOException {
        setRequest(request, saveDirectory, maxPostSize, encoding, null);
    }
    /** HttpServletRequestEmulatorクラス構築用メソッド
     * インスタンス生成後に呼出し可能
     * @param   request         (HttpServletRequest)
     * @param   saveDirectory   (String)保存ディレクトリ
     * @param   maxPostSize     (int)アップロードサイズ
     * @param   encoding        (String)エンコーディング
     * @param   policy          (FileRenamePolicy)ファイル名重複防止クラス
     * @throws  IOException     (IOException)
     */
    public void setRequest(HttpServletRequest request, String saveDirectory, int maxPostSize, String encoding, FileRenamePolicy policy) throws IOException {
        
        this.request = request;
        
        params = new Hashtable();   //パラメータ文字列
        files = new Hashtable();    //アップロードファイル名

        // 引数チェック
        if (request == null) {
            System.out.println("HttpServletRequestEmulator : setRequest() 引数 'request' が null です。");
            throw new IllegalArgumentException("HttpServletRequestEmulator : setRequest() 引数 'request' が null です。");
        }
        if (maxPostSize <= 0) {
            System.out.println("HttpServletRequestEmulator : setRequest() 引数 'maxPostSize' は 0 より大きい数を指定しなければなりません。");
            throw new IllegalArgumentException("HttpServletRequestEmulator : setRequest() 引数 'maxPostSize' は 0 より大きい数を指定しなければなりません。");
        }
        if (saveDirectory == null) {
            System.out.println("HttpServletRequestEmulator : setRequest() 引数 'saveDirectory' が null です。");
            throw new IllegalArgumentException("HttpServletRequestEmulator : setRequest() 引数 'saveDirectory' が null です。");
        }
        
        //エンコーディングの設定(null ならばデフォルトを使用)
        if (encoding != null) {
            this.strEncoding = encoding;
        }
        
        //マルチパートでは無い場合、パラメータのみ取得してリターン
        if ((request.getContentType() != null && !request.getContentType().startsWith("multipart/form-data"))
            || request.getContentType() == null) {
            setRequestParameter(request);
            return;
        }
        
        File dir = new File(saveDirectory);

        // ディレクトリかどうか
        if (!dir.isDirectory()) {
            System.out.println("HttpServletRequestEmulator : setRequest() '" + saveDirectory + "' がディレクトリではありません。");
            throw new IllegalArgumentException("HttpServletRequestEmulator : setRequest() '" + saveDirectory + "' がディレクトリではありません。");
        }

        // 書込み可能かどうか
        if (!dir.canWrite()) {
            System.out.println("HttpServletRequestEmulator : setRequest() '" + saveDirectory + "' が書込み不可です。");
            throw new IllegalArgumentException("HttpServletRequestEmulator : setRequest() '" + saveDirectory + "' が書込み不可です。");
        }

        // マルチパートを解析し、ファイルを指定ディレクトリへ保存する
        MultipartParser parser = new MultipartParser(request, maxPostSize, true, true, this.strEncoding);

        //requestからパラメータを取得
        setRequestParameter(request);

        //パートを取得
        Part part;
        while ((part = parser.readNextPart()) != null) {
            String name = part.getName();
            
            if (part.isFile()) {
                // ファイルパート
                FilePart filePart = (FilePart) part;
                String fileName = filePart.getFileName();
                if (fileName != null) {
                    //リクエストのVALUEをそのままファイル名には使わず、代わりに日時(long)+RenamePolicyを使う
                    filePart.setFileName ("" + System.currentTimeMillis ()+(fileName.lastIndexOf(".")==-1?"":fileName.substring(fileName.lastIndexOf(".")).toLowerCase()));
                    // ファイルが含まれている
                    filePart.setRenamePolicy(policy);  // ファイル名重複を防ぐ
                    filePart.writeTo(dir);
                    files.put(name, new UploadedFile(dir.toString(), filePart.getFileName(), fileName, filePart.getContentType()));
                } else { 
                    // ファイルを含んでいない
                    //files.put(name, new UploadedFile(null, null, null, null));
                }
                
            } else if (part.isParam()) {
                // パラメータパート
                ParamPart paramPart = (ParamPart) part;
                String value = paramPart.getStringValue();
                Vector existingValues = (Vector) params.get(name);
                if (existingValues == null) {
                    existingValues = new Vector();
                    params.put(name, existingValues);
                }
                existingValues.addElement(value);
            }
        }
    }
    
    /** リクエストパラメータを設定
     * @param   request         (ServletRequest)
     * @throws  IOException     (IOException)
     */
    private void setRequestParameter(HttpServletRequest request) throws IOException {

        //comment out by kubo. 20041222
        //if (request.getQueryString() != null) {
            // 全てのパラメータ名を取得
            System.out.println ("user-agent is " + request.getHeader ("user-agent"));
            System.out.println ("characterencoding is " + request.getCharacterEncoding ());
            Enumeration queryParameterNames = request.getParameterNames();
            while (queryParameterNames.hasMoreElements()) {
                String name = (String) queryParameterNames.nextElement();
                String value = request.getParameter(name).trim();
                //System.out.println ("name is " + name + " and value is " + value);
                try {
                    if (request.getCharacterEncoding () == null) {
                        value = new String (value.getBytes ("8859_1"), this.strEncoding);
                    }
                } catch (IOException ie) {
                    String debugString = ie.getLocalizedMessage();
                    throw new IOException("HttpServletRequestEmulator : setRequestParameter() " + debugString);
                }
                Vector newValues = new Vector();
                if (request.getParameterValues (name).length == 1) {
                    newValues.add(value);
                } else {
                    String [] values = request.getParameterValues (name);
                    for (int i = 0; i < values.length; i++) {
                        if (request.getCharacterEncoding () == null) {
                            values[i] = new String (values[i].getBytes ("8859_1"), this.strEncoding);
                        }
                        newValues.add (values [i]);
                    }
                }
                params.put(name, newValues);
            }
        //comment out by kubo. 20041222
        //}
    }
    
    /** テンポラリディレクトリを作成
     * @param   strPath     (String)このパス以下にmakeDirで指定したディレクトリを作成する
     * @param   makeDir     (String)作成するディレクトリ名
     * @return  (String)    作成したディレクトリへのパス
     */
    private static String makeTempDir(String strPath, String makeDir) {
        
        File fileDir = new File(strPath, "work");
        //存在しない、又はディレクトリでなければ不正
        if (!fileDir.exists() || !fileDir.isDirectory()) {
            return  null;
        }
        
        fileDir = new File(fileDir.getAbsoluteFile (), makeDir);
        //存在してなければ作成
        if (!fileDir.exists()) {
            if (!fileDir.mkdir()) {
                //作成できなかった
                return  null;
            }
        }
        return  fileDir.getPath();
    }
    
    /** パラメータ要素名取得
     * @return  (Enumeration)   パラメータ要素名
     */
    public Enumeration getParameterNames() {
        return params.keys();
    }

    /** ファイル要素名取得
     * @return  (Enumeration)   ファイル要素名
     */
    public Enumeration getFileNames() {
        return files.keys();
    }

    /** パラメータ値取得
     * @param   name        (String)パラメータ要素名
     * @return  (String)    パラメータ値
     */
    public String getParameter(String name) {
        try {
            Vector values = (Vector) params.get(name);
            if (values == null || values.size() == 0) {
                return "";
            }
            String value = (String) values.elementAt(values.size() - 1);
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /** パラメータ値配列取得
     * @param   name        (String)パラメータ要素名
     * @return  (String[])  パラメータ値配列
     */
    public String[] getParameterValues(String name) {
        try {
            Vector values = (Vector) params.get(name);
            if (values == null || values.size() == 0) {
                return null;
            }
            String[] valuesArray = new String[values.size()];
            values.copyInto(valuesArray);
            return valuesArray;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isFilePart (String name) {
        if (files.get (name) == null) {
            return false;
        } else {
            return true;
        }
    }
    
    /** ファイル名取得
     * @param   name        (String)ファイル要素名
     * @return  (String)    ファイル名
     */
    public String getFilesystemName(String name) {
        try {
            UploadedFile file = (UploadedFile) files.get(name);
            return file.getFilesystemName() != null ? file.getFilesystemName() : "";
        } catch (Exception e) {
            return "";
        }
    }

    /** 変更前ファイル名取得
     * @param   name        (String)ファイル要素名
     * @return  (String)    変更前ファイル名
     */
    public String getOriginalFileName(String name) {
        try {
            UploadedFile file = (UploadedFile) files.get(name);
            return file.getOriginalFileName() != null ? file.getOriginalFileName() : "";
        } catch (Exception e) {
            return "";
        }
    }

    /** コンテンツタイプ取得
     * @param   name        (String)ファイル要素名
     * @return  (String)    コンテンツタイプ
     */
    public String getContentType(String name) {
        try {
            UploadedFile file = (UploadedFile) files.get(name);
            return file.getContentType() != null ? file.getContentType() : "";
        } catch (Exception e) {
            return "";
        }
    }

    /** アップロードファイル取得
     * @param   name        (String)ファイル要素名
     * @return  (File)      アップロードファイル
     */
    public File getFile(String name) {
        try {
            UploadedFile file = (UploadedFile) files.get(name);
            return file.getFile();
        } catch (Exception e) {
            return null;
        }
    }
    
    /** アップロードファイルのサイズ取得
     * @param   name        (String)ファイル要素名
     * @return  (long)      アップロードファイルのサイズ
     */
    public long getFileSize(String name) {
        try {
            UploadedFile file = (UploadedFile) files.get(name);
            return file.getFileSize();
        } catch (Exception e) {
            return -1;
        }
    }

    /** 標準アップロードパスの取得メソッド
     * @return  (String)    標準アップロードパス
     */
    public static java.lang.String getDefaultSavePath() {
        return defaultSavePath;
    }
    
    
    public HttpServletRequest getHttpServletRequest(){
        return request;
    }
}
