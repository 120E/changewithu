/*
 * DefaultFileRenamePolicy.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.File;
import java.io.IOException;

/** 衝突するファイル名に増加する整数を加え、改名します。
 * 例えば、foo.gifがアップロードされており、同じ名前によるファイルが既に存在する場合、
 * このロジックはfoo1.gifと改名するでしょう。
 * 同じ名前による別のアップロードはfoo2.gifになるでしょう。
 * @author  FFT corp.
 */
public class DefaultFileRenamePolicy implements FileRenamePolicy {
    
    /** ファイル名変更
     * @param   f       (File)変更元ファイル
     * @return  (File)  変更後ファイル
     */
    public File rename(File file) {
        if (createNewFile(file)) {
            return file;
        }
        
        String strName = file.getName();
        String strBody = null;
        String strExt = null;

        int intDot = strName.lastIndexOf(".");
        if (intDot != -1) {
            strBody = strName.substring(0, intDot);  //ベースとなるファイル名
            strExt = strName.substring(intDot);      // 拡張子
        } else {
            strBody = strName;
            strExt = "";
        }

        // 使用されていないファイル名が見つかるまで、カウンタを増加します。
        // 無限ループを回避するために最大値を10000とします。
        int intCount = 0;
        while (!createNewFile(file) && intCount < 10000) {
            intCount++;
            String newName = strBody + intCount + strExt;
            file = new File(file.getParent(), newName);
        }
        return file;
    }

    /** サイズがゼロのファイルを作成
     * ファイル名が既存ならば false をリターンする
     * @param   f           (File)作成するファイル
     * @return  (boolean)   true = 成功、false = 失敗
     */
    private boolean createNewFile(File file) {
        try {
            return  file.createNewFile();
        } catch (IOException ignored) {
            return  false;
        }
    }

}
