/*
 * LimitedServletInputStream.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.IOException;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

/** LimitedServletInputStreamクラスは、どれだけのバイトが読まれたか、また、
 * Content-Length の限界にいつ到達したかを検知して跡を追うために、別の
 * ServletInputStreamをラップするクラスです。
 * @author  FFT corp.
 */
public class LimitedServletInputStream extends ServletInputStream {

    /** ServletInputStreamクラス */
    private ServletInputStream sis;
    /** 読込むバイト数の限界 */
    private int intTotalExpected;
    /** 読込んだバイト数 */
    private int intTotalReadSize = 0;

    /** LimitedServletInputStreamクラスコンストラクタ
     * ServletInputStreamをラップする、指定された長さの限界を備えたクラスを作成します。
     * @param   in              (ServletInputStream)
     * @param   totalExpected   (int)読込むバイト数の限界
     */
    public LimitedServletInputStream(ServletInputStream in, int totalExpected) {
        this.sis = in;
        this.intTotalExpected = totalExpected;
    }

    /** 読込む長さを制限する、ラップされたServletInputStreamのreadLineメソッド
     * @param   b           (byte[])読込バッファ
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @return  (int)       読込バイト数、又はストリームが終了している場合 -1
     * @throws  IOException (IOException)
     */
    public int readLine(byte b[], int off, int len) throws IOException {
        int result, left = intTotalExpected - intTotalReadSize;
        if (left <= 0) {
            return -1;
        } else {
            result = ((ServletInputStream) sis).readLine(b, off, Math.min(left, len));
        }
        if (result > 0) {
            intTotalReadSize += result;
        }
        return result;
    }

    /** 読込む長さを制限する、ラップされたServletInputStreamのreadメソッド
     * @return  (int)       読込バイト数、又はストリームが終了している場合 -1
     * @throws  IOException (IOException)
     */
    public int read() throws IOException {
        if (intTotalReadSize >= intTotalExpected) {
            return -1;
        }

        int result = sis.read();
        if (result != -1) {
            intTotalReadSize++;
        }
        return result;
    }

    /** 読込む長さを制限する、ラップされたServletInputStreamのreadメソッド
     * @param   b           (byte[])読込バッファ
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @return  (int)       読込バイト数、又はストリームが終了している場合 -1
     * @throws  IOException (IOException)
     */
    public int read(byte b[], int off, int len) throws IOException {
        int result, left = intTotalExpected - intTotalReadSize;
        if (left <= 0) {
            return -1;
        } else {
            result = sis.read(b, off, Math.min(left, len));
        }
        if (result > 0) {
            intTotalReadSize += result;
        }
        return result;
    }

    @Override
    public boolean isFinished() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setReadListener(ReadListener rl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
