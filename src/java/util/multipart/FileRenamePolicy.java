/*
 * FileRenamePolicy.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.File;

/**
 * FileRenamePolicyクラスインターフェース
 * @author  FFT corp.
 */
public interface FileRenamePolicy {
    
    /** ファイル名変更インターフェース
     * @param   f       (file)変更元ファイル
     * @return  (File)  変更後ファイル
     */
    File rename(File file);
}
