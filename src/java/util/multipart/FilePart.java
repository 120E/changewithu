/*
 * FilePart.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedOutputStream;
import javax.servlet.ServletInputStream;

/** FilePartクラス
 * FilePartクラスは<INPUT TYPE='file'>形式パラメーターを表します。
 * ファイルアップロードデータが単一のInputStreamによって到着するので、
 * 次の部分上に移動する前に、各FilePartの内容が読まれるに違いないことに注意してください。
 * @author  FFT corp.
 */
public class FilePart extends Part {

    /** ファイル名 */
    private String strFileName;
    /** パス */
    private String strFilePath;
    /** コンテンツタイプ */
    private String strContentType;
    /** PartInputStreamクラス */
    private PartInputStream partInput;
    /** ファイル名重複防止クラス */
    private FileRenamePolicy fRenamePolicy;

    /** FilePartクラスコンストラクタ
     * @param   name            (String)キー
     * @param   in              (ServletInputStream)
     * @param   boundary        (String)
     * @param   contentType     (String)コンテンツタイプ
     * @param   fileName        (String)ファイル名
     * @param   filePath        (String)パス
     * @throws  IOException     (IOException)
     */
    public FilePart(String name, ServletInputStream in, String boundary,
                    String contentType, String fileName, String filePath)
                    throws IOException {
                        
        super(name);
        this.strFileName = fileName;
        this.strFilePath = filePath;
        this.strContentType = contentType;
        partInput = new PartInputStream(in, boundary);
    }

    /** ファイル名重複防止クラスの設定
     * @param   policy  (FileRenamePolicy)ファイル名重複防止クラス
     */
    public void setRenamePolicy(FileRenamePolicy policy) {
        this.fRenamePolicy = policy;
    }
  
    /** ファイル名を設定
     * @param value    ファイル名
     */
    public void setFileName(String value) {
        this.strFileName = value;
    }

    /** ファイル名を取得
     * @return  (String)    ファイル名
     */
    public String getFileName() {
        return strFileName;
    }

    /** パスを取得
     * @return  (String)    パス
     */
    public String getFilePath() {
        return strFilePath;
    }

    /** コンテンツタイプを取得
     * @return  (String)    コンテンツタイプ
     */
    public String getContentType() {
        return strContentType;
    }

    /** フィールドのpartInputを取得
     * @return  (InputStream)   フィールドのpartInput
     */
    public InputStream getInputStream() {
        return partInput;
    }

    /** ファイルをディスクに書込む
     * @param   fileOrDirectory     (File)ファイル又はディレクトリ
     * @return  (long)              書込んだサイズ
     * @throws  IOException         (IOException)
     */
    public long writeTo(File fileOrDirectory) throws IOException {
        long written = 0;
    
        OutputStream fileOut = null;
        try {
            if (strFileName != null) {
                // Check if user supplied directory
                File file;
                if (fileOrDirectory.isDirectory()) {
                    file = new File(fileOrDirectory, strFileName);
                } else {
                    file = fileOrDirectory;
                }
                if (fRenamePolicy != null) {
                    file = fRenamePolicy.rename(file);
                    strFileName = file.getName();
                }
                fileOut = new BufferedOutputStream(new FileOutputStream(file));
                written = write(fileOut);
            }
        } finally {
            if (fileOut != null) {
                fileOut.close();
            }
        }
        return written;
    }

    /** 渡されたOutputStreamを使用してファイルをディスクに書込む
     * @param   out     (OutputStream)
     * @return  (long)  書込んだサイズ
     * @throws  IOException         (IOException)
     */
    public long writeTo(OutputStream out) throws IOException {
        long size = 0;
        if (strFileName != null) {
            size = write(out);
        }
        return size;
    }

    /** ファイルをディスクに書込む内部的なメソッド
     * ファイルの内容を持っているかどうかはチェックされない
     * @param   out     (OutputStream)
     * @return  (long)  書込んだサイズ
     * @throws  IOException         (IOException)
     */
    long write(OutputStream out) throws IOException {
        // もしこれが送られた場合、マックバイナリデコードする
        if (strContentType.equals("application/x-macbinary")) {
          out = new MacBinaryDecoderOutputStream(out);
        }
        long size = 0;
        int read;
        byte[] buf = new byte[8 * 1024];
        while ((read = partInput.read(buf)) != -1) {
            out.write(buf, 0, read);
            size += read;
        }
        return size;
    }
  
    /**
     * ファイルかどうか
     * @return  boolean     true = ファイル、false = ファイルではない
     */
    public boolean isFile() {
        return true;
    }

}
