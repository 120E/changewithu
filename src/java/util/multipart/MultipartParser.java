/*
 * MultipartParser.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.util.Vector;
import java.util.Enumeration;
import java.io.IOException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

/** multipart/form-data形式リクエストを扱うユーティリィティクラス。
 * ファイルを支援するリクエストは、アップロードされます。
 * あなたがHttpServletRequestに似ているAPIを使用したい場合は、
 * MultipartRequest(現在はクラス名が変更されている)を代わりに使用してください。
 * それは本クラスのラッパークラスです。
 * 本クラスは任意に大きなファイルを(人工限界まで)かなり効率的に受け取ることができます。
 * 入れ子のデータ(マルチパート内のマルチパート)を扱うことができません。
 * ファイル名に使用できる文字を国際化しました。
 * @author  FFT corp.
 */
public class MultipartParser {
    
    /** ServletInputStreamクラス */
    private ServletInputStream sis;
    /**  */
    private String strBoundary;
    /** FilePartクラス */
    private FilePart lastFilePart;
    /** 読込み用バッファ 100K */
    private byte[] byteBuf = new byte[1024*100];
    /** デフォルトエンコーディング */
    private static String strDefaultEncoding = "utf-8";//"ISO-8859-1";
    /** エンコーディング */
    private String strEncoding = strDefaultEncoding;

    /** MultipartParserクラスコンストラクタ
     * 指定されたリクエストからMultipartParserを作成します。
     * アップロード・サイズを指定された長さに制限します。
     * @param   req         (HttpServletRequest)
     * @param   maxSize     (int)アップロードサイズ
     * @throws  IOException (IOException)
     */
    public MultipartParser(HttpServletRequest req, int maxSize) throws IOException {
        this(req, maxSize, true, true);
    }
    /** MultipartParserクラスコンストラクタ
     * 指定されたリクエストからMultipartParserを作成します。
     * アップロード・サイズを指定された長さに制限します。
     * @param   req         (HttpServletRequest)
     * @param   maxSize     (int)アップロードサイズ
     * @param   buffer      (boolean)
     * @param   limitLength (boolean)
     * @throws  IOException (IOException)
     */
    public MultipartParser(HttpServletRequest req, int maxSize, boolean buffer, boolean limitLength) throws IOException {
        this(req, maxSize, buffer, limitLength, null);
    }
    /** MultipartParserクラスコンストラクタ
     * 指定されたリクエストからMultipartParserを作成します。
     * アップロード・サイズを指定された長さに制限します。
     * @param   req         (HttpServletRequest)
     * @param   maxSize     (int)アップロードサイズ
     * @param   buffer      (boolean)
     * @param   limitLength (boolean)
     * @param   encoding    (String)エンコーディング
     * @throws  IOException (IOException)
     */
    public MultipartParser(HttpServletRequest req, int maxSize, boolean buffer, boolean limitLength, String encoding) throws IOException {
        if (encoding != null) {
            setEncoding(encoding);
        }

        //multipart/form-data であることを確かめるためにContent-Typeをチェックします。
        String type = null;
        String type1 = req.getHeader("Content-Type");
        String type2 = req.getContentType();
        //1つの値が無効の場合は、別の値を選ぶ
        if (type1 == null && type2 != null) {
            type = type2;
        } else {
            if (type2 == null && type1 != null) {
                type = type1;
            } else {
                //どちらの値も無効ではない場合は、より長い値を選ぶ
                if (type1 != null && type2 != null) {
                    type = (type1.length() > type2.length()) ? type1 : type2;
                }
            }
        }

        if (type == null || !type.toLowerCase().startsWith("multipart/form-data")) {
            throw new IOException("MultipartParser : MultipartParser() ポストされた Content-Type が multipart/form-data ではありません。");
        }

        //アップロードサイズのチェック
        int length = req.getContentLength();
        if (length > maxSize) {
            throw new IOException("MultipartParser : MultipartParser() ポストされたアップロードサイズ (" + length + ") が、上限 (" + maxSize + ") を超えています。");
        }

        //マルチパートリクエストの境界文字を得る
        String boundary = extractBoundary(type);
        if (boundary == null) {
            throw new IOException("MultipartParser : MultipartParser() 境界文字 (boundary) を取得できませんでした。");
        }

        ServletInputStream in = req.getInputStream();
    
        // If required, wrap the real input stream with classes that "enhance" its behaviour for performance and stability
        if (buffer) {
            in = new BufferedServletInputStream(in);
        }
        if (limitLength) {
            in = new LimitedServletInputStream(in, length);
        }

        //フィールドへ保存
        this.sis = in;
        this.strBoundary = boundary;
    
        //境界文字が検出されるまで読込みます。
        //いくつかのクライアントはpreamble(RFC 2046)を送ります。したがって、それを無視してください。
        do {
            String line = readLine();
            if (line == null) {
                throw new IOException("MultipartParser : MultipartParser() readLine メソッドが、境界文字 (boundary) を検出する前に null を返しました。");
            }
            //境界文字が検出されたらbreakする
            if (line.startsWith(boundary)) {
                break;  //成功
            }
        } while (true);
    }

    /** エンコーディングを設定
     * @param   encoding    (String)エンコーディング
     */
    public void setEncoding(String encoding) {
        this.strEncoding = encoding;
    }

    /** 次のパートを読込む
     * @return  (Part)      Partクラス
     * @throws  IOException (IOException)
     */
    public Part readNextPart() throws IOException {
        if (lastFilePart != null) {
            lastFilePart.getInputStream().close();
            lastFilePart = null;
        }
    
        //ヘッダを読むとこれらの様に見えます。
        //Content-Disposition: form-data; name="field1"; filename="file1.txt"
        //Content-Type: type/subtype
        //Content-Transfer-Encoding: binary
        Vector headers = new Vector();

        String line = readLine();
        if (line == null) {
            return null;
        } else {
            if (line.length() == 0) {
                //MacのIE4は終わりで空のラインを送ります。それを終了として扱ってください。
                return null;
            }
        }

        //空のラインが検出されたら次のヘッダー・ラインを読みます。
        //余白で始まるラインは継続と考えられます。
        //それは少し特別のロジックを要求します。
        while (line != null && line.length() > 0) {
            String nextLine = null;
            boolean getNextLine = true;
            while (getNextLine) {
                nextLine = readLine();
                if (nextLine != null && (nextLine.startsWith(" ") || nextLine.startsWith("\t"))) {
                  line = line + nextLine;
                } else {
                    getNextLine = false;
                }
            }
            // ヘッダーリストへラインを追加
            headers.addElement(line);
            line = nextLine;
        }

        //nullのラインを得たならば、終了です。
        if (line == null) {
            return null;
        }

        String name = null;
        String filename = null;
        String origname = null;
        String contentType = "text/plain";  //rfc1867では、これがデフォルトです。

        Enumeration enm = headers.elements();
        while (enm.hasMoreElements()) {
            String headerline = (String) enm.nextElement();
            if (headerline.toLowerCase().startsWith("content-disposition:")) {
                //content-dispositionのラインを解析します。
                String[] dispInfo = extractDispositionInfo(headerline);
                // String disposition = dispInfo[ 0 ];  //現在使用されていません。
                name = dispInfo[1];
                filename = dispInfo[2];
                origname = dispInfo[3];
            } else {
                if (headerline.toLowerCase().startsWith("content-type:")) {
                    //どれも指定しなかった場合、content-type、あるいはnullを得ます。
                    String type = extractContentType(headerline);
                    if (type != null) {
                        contentType = type;
                    }
                }
            }
        }

        //最後に、内容を読みます。
        if (filename == null) {
            //これはパラメーターです。Vectorにそれを加えます。
            //encodingは値を解析することを支援するために必要です。
            return new ParamPart(name, sis, strBoundary, strEncoding);
        } else {
            //これはファイルです。
            if (filename.equals("")) {
                filename = null; //空のファイル名(恐らく「空の」ファイルパラメータ)
            }
            lastFilePart = new FilePart(name, sis, strBoundary, contentType, filename, origname);
//System.out.println("---------------"+lastFilePart.getFileName()+"  "+lastFilePart.getContentType());
            return lastFilePart;
        }
    }
  
    /** ラインからboundaryを解析し、返します。
     * @param   line        (String)ライン文字列
     * @return  (String)    境界文字
     */
    private String extractBoundary(String line) {
        //Win98の上のIE 4.01は「boundary=」文字列を複数回送ると知られているので、
        //lastIndexOf()を使用します。
        int index = line.lastIndexOf("boundary=");
        if (index == -1) {
            return null;
        }
        String boundary = line.substring(index + 9);  // 9 は "boundary=" の文字数
        if (boundary.charAt(0) == '"') {
            //境界文字は "" で囲まれ、その中身を得ます。
            index = boundary.lastIndexOf('"');
            boundary = boundary.substring(1, index);
        }

        //実際の境界文字は、"--" から始まります。
        boundary = "--" + boundary;

        return boundary;
    }

    /** 要素を備えたStringの配列として、ラインから配置情報を抽出して返します。
     * disposition, name, filename
     * @param   line        (String)ライン文字列
     * @return  (String)    配置情報
     * @throws  IOException (IOException)
     */
    private String[] extractDispositionInfo(String line) throws IOException {
        //ラインのデータを配列として返します。
        //disposition, name, filename
        String[] retval = new String[4];

        //ラインを \r\n のない小文字の文字列に変換します。
        //元の文字列を origline にキープします。
        String origline = line;
        line = origline.toLowerCase();

        // content-disposition を得ます。"form-data" であるべきです。
        int start = line.indexOf("content-disposition: ");
        int end = line.indexOf(";");
        if (start == -1 || end == -1) {
            throw new IOException("MultipartParser : extractDispositionInfo() content-disposition が不正です。(" + origline + ")");
        }
        String disposition = line.substring(start + 21, end);
        if (!disposition.equals("form-data")) {
            throw new IOException("MultipartParser : extractDispositionInfo() content-disposition が、'form-data' ではありません。(" + disposition + ")");
        }

        // name を得ます。
        start = line.indexOf("name=\"", end);
        end = line.indexOf("\"", start + 7);
        int startOffset = 6;
        if (start == -1 || end == -1) {
            //lynx のようないくつかのブラウザーは""で囲みません。
            start = line.indexOf("name=", end);
            end = line.indexOf(";", start + 6);
            if (start == -1) {
                throw new IOException("MultipartParser : extractDispositionInfo() name が不正です。(" + origline + ")");
            } else {
                if (end == -1) {
                    end = line.length();
                }
                startOffset = 5;  // " を１つ除いたオフセット
            }
        }
        String name = origline.substring(start + startOffset, end);

        //もし与えられれば、ファイル名を得ます。
        String filename = null;
        String origname = null;
        start = line.indexOf("filename=\"", end + 2);
        end = line.indexOf("\"", start + 10);
        if (start != -1 && end != -1) {
            filename = origline.substring(start + 10, end);
            origname = filename;
            //ファイル名は十分なパスを含んでいるかもしれません。単なるファイル名に切断します。
            int slash = Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
            if (slash > -1) {
                filename = filename.substring(slash + 1);
            }
        }

        //ストリングの配列を返します。disposition, name, filename
        //filenameが空の場合、ファイル名、ファイルは記入しません。
        retval[0] = disposition;
        retval[1] = name;
        retval[2] = filename;
        retval[3] = origname;
        return retval;
    }

    /** ラインからコンテンツタイプを返します。あるいはラインが空だった場合、空を抽出し返します。
     * @param   line        (String)ライン文字列
     * @return  (String)    コンテンツタイプ
     * @throws  IOException (IOException)
     */
    private static String extractContentType(String line) throws IOException {
        //ラインを小文字のストリングに変換します。
        line = line.toLowerCase();

        //オペラがContent-Typeの後に余分な情報を置くことに注目する場合は、
        //Content-Typeを得る為に、それを扱ってください。
        //例）Content-Type: text/plain; name="foo"
        int end = line.indexOf(";");
        if (end == -1) {
            end = line.length();
        }

        return line.substring(13, end).trim();  // "content-type:" is 13
    }
  
    /** 次のラインを読みます。
     * @return  (String)    ライン文字列
     * @throws  IOException (IOException)
     */
    private String readLine() throws IOException {
        StringBuffer sbuf = new StringBuffer();
        int result;
        String line;

        do {
            result = sis.readLine(byteBuf, 0, byteBuf.length);
            if (result != -1) {
                sbuf.append(new String(byteBuf, 0, result, strEncoding));
            }
        } while (result == byteBuf.length);  //バッファーが満たされた場合、ループします。

        if (sbuf.length() == 0) {
            return null;  //読込まれたサイズがゼロの場合、終わりに間違いない。
        }

        // "\n" あるいは "\r\n" をカットします。
        //それらは常に "\r\n" であるべきですが、IE5は時々単なる "\n" にします。
        int len = sbuf.length();
        if (len >= 2 && sbuf.charAt(len - 2) == '\r') {
            sbuf.setLength(len - 2);
        } else {
            if (len >= 1 && sbuf.charAt(len - 1) == '\n') {
                sbuf.setLength(len - 1);
            }
        }
        return sbuf.toString();
    }
    
}
