/*
 * MacBinaryDecoderOutputStream.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.IOException;
import java.io.OutputStream;
import java.io.FilterOutputStream;

/** MacBinaryDecoderOutputクラス
 * マックバイナリファイルを正常なファイルにフィルターするクラス
 * @author  FFT corp.
 */
public class MacBinaryDecoderOutputStream extends FilterOutputStream {
    
    /** フィルタ済みバイト数 */
    private int intBytesFiltered = 0;
    /** データフォークの長さ */
    private int intDataForkLength = 0;

    /** MacBinaryDecoderOutputStreamクラスコンストラクタ
     * @param   out     (OutputStream)
     */
    public MacBinaryDecoderOutputStream(OutputStream out) {
        super(out);
    }

    /** writeメソッドのオーバーライド
     * @param   b           (int)書込むバイト
     * @throws  IOException (IOException)
     */
    public void write(int b) throws IOException {
        if (intBytesFiltered <= 86 && intBytesFiltered >= 83) {
            int leftShift = (86 - intBytesFiltered) * 8;
            intDataForkLength = intDataForkLength | (b & 0xff) << leftShift;
        } else {
            if (intBytesFiltered < (128 + intDataForkLength) && intBytesFiltered >= 128) {
                out.write(b);
            }
        }
        intBytesFiltered++;
    }

    /** writeメソッドのオーバーライド
     * @param   b           (byte[])書込むバイト配列
     * @throws  IOException (IOException)
     */
    public void write(byte b[]) throws IOException {
        write(b, 0, b.length);
    }

    /** writeメソッドのオーバーライド
     * @param   b           (byte[])書込むバイト配列
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @throws  IOException (IOException)
     */
    public void write(byte b[], int off, int len) throws IOException {
        //データ・フォークの端を過ぎている場合、無視する。
        if (intBytesFiltered >= (128 + intDataForkLength)) {
            intBytesFiltered += len;
        } else {
            //データ・フォーク内にある場合は、それを直接書いてください。
            if (intBytesFiltered >= 128 && (intBytesFiltered + len) <= (128 + intDataForkLength)) {
                out.write(b, off, len);
                intBytesFiltered += len;
            } else {
                //そうでなければ、1バイトづつ書き込みを行ってください。
                for (int i = 0 ; i < len ; i++) {
                    write(b[off + i]);
                }
            }
        }
    }
    
}
