/*
 * UploadedFile.java
 *
 * Created on 2004/04/27, 18:43
 */

package util.multipart;

import java.io.File;

/** アップロードされたファイルに関する情報を保持するクラス
 * @author   FFT corp.
 */
public class UploadedFile {

    /** ディレクトリ */
    private String strDir;
    /** ファイル名 */
    private String strFilename;
    /** 変更前ファイル名 */
    private String strOriginal;
    /** コンテンツタイプ */
    private String strType;

    /** UploadedFileクラスコンストラクタ
     * @param   dir         (String)ディレクトリ
     * @param   filename    (String)ファイル名
     * @param   original    (String)変更前ファイル名
     * @param   type        (String)コンテンツタイプ
     */
    public UploadedFile(String dir, String filename, String original, String type) {
        this.strDir = dir;
        this.strFilename = filename;
        this.strOriginal = original;
        this.strType = type;
    }

    /** コンテンツタイプ取得
     * @return  (String)    コンテンツタイプ
     */
    public String getContentType() {
        return strType;
    }

    /** ファイル名取得
     * @return  (String)    ファイル名
     */
    public String getFilesystemName() {
        return strFilename;
    }

    /** 変更前ファイル名取得
     * @return  (String)    変更前ファイル名
     */
    public String getOriginalFileName() {
        return strOriginal;
    }

    /** ファイル取得
     * @return  (File)    ファイル
     */
    public File getFile() {
        if (strDir == null || strFilename == null) {
            return null;
        } else {
            return new File(strDir + File.separator + strFilename);
        }
    }
    
    /** ファイルサイズ取得
     * @return  (long)    ファイルサイズ
     */
    public long getFileSize() {
        if (strDir == null || strFilename == null) {
            return -1;
        } else {
            return (new File(strDir + File.separator + strFilename)).length();
        }
    }
}
