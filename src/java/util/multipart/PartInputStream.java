/*
 * PartInputStream.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.IOException;
import java.io.FilterInputStream;
import javax.servlet.ServletInputStream;

/** ServletInputStreamをフィルターするクラス
 * 境界文字を指定し、含まれていた単一の MIME part へのアクセスを提供します。
 * それはバッファーを使用して最大性能を提供します。
 * ServletInputStreamのreadLineメソッドが、最後のラインの終了へ \r\n を加える習慣
 * を持っていることに注意してください。
 * @author  FFT corp.
 */
public class PartInputStream extends FilterInputStream {

    /** 境界文字 */
    private String strBoundary;
    /** バッファ　64k */
    private byte[] byteBuffer = new byte[65536];
    /** バッファーへ読み込んだバイト数 */
    private int intCount; 
    /** バッファー中の現在の位置 */
    private int intPos;
    /** 終了フラグ */
    private boolean bEOF;
    
    /** PartInputStreamクラスコンストラクタ
     * ServletInputStreamから、指定された境界で止まるPartInputStreamを作成します。
     * @param   in          (ServletInputStream)
     * @param   boundary    (String)境界文字
     * @throws  IOException (IOException)
     */
    PartInputStream(ServletInputStream in, String boundary) throws IOException {
        super(in);
        this.strBoundary = boundary;
    }

    /** ServletInputStreamからバッファーへ読込み、ファイルの端を示す境界をチェックします。
     * @throws  IOException (IOException)
     */
    private void fill() throws IOException {
        if (bEOF) {
            return;
        }
    
        // 開始直後ではない場合
        if (intCount > 0) {
            // 必要な量をバッファーにおいて予備にしておいた場合
            if (intCount - intPos == 2) {
                // バッファーのスタートにそれをコピーします。
                System.arraycopy(byteBuffer, intPos, byteBuffer, 0, intCount - intPos);
                intCount -= intPos;
                intPos = 0;
            } else {
                throw new IllegalStateException("PartInputStream : fill() バッファエラーを検出");
            }
        }
    
        //全バッファーを満たす為に、countからスタートします。
        //一行ずつ読込むが、しかし、実際はそのように読みません。
        //それはバッファの終了へ接近しているからです。
        //境界文字さえ分割される場合もあります。
        int read = 0;
        int boundaryLength = strBoundary.length();
        int maxRead = byteBuffer.length - boundaryLength - 2;  // -2 は \r\n
        while (intCount < maxRead) {
            // １行読込
            read = ((ServletInputStream) in).readLine(byteBuffer, intCount, byteBuffer.length - intCount);
            // 境界文字と終端チェック
            if (read == -1) {
                throw new IOException("PartInputStream : fill() 境界文字が検出される前に part が終了しました。");
            } else {
                if (read >= boundaryLength) {
                    bEOF = true;
                    for (int i = 0; i < boundaryLength; i++) {
                        if (strBoundary.charAt(i) != byteBuffer[intCount + i]) {
                            // 境界文字ではない
                            bEOF = false;
                            break;
                        }
                    }
                    if (bEOF) {
                        break;
                    }
                }
            }
            // 成功
            intCount += read;
        }
    }
  
    /** InputStream クラス read 抽象メソッドの実装
     * @return  (int)       -1 = このパートのMIME境界に遭遇する場合(end of file)
     * @throws  IOException (IOException)
     */
    public int read() throws IOException {
        if (intCount - intPos <= 2) {
            fill();
            if (intCount - intPos <= 2) {
                return -1;
            }
        }
        return byteBuffer[intPos++] & 0xff;
    }

    /** InputStream クラス read 抽象メソッドの実装
     * @param   b           (byte[])読込バッファ
     * @return  (int)       -1 = このパートのMIME境界に遭遇する場合(end of file)
     * @throws  IOException (IOException)
     */
    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    /** InputStream クラス read 抽象メソッドの実装
     * @param   b           (byte[])読込バッファ
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @return  (int)       -1 = このパートのMIME境界に遭遇する場合(end of file)
     * @throws  IOException (IOException)
     */
    public int read(byte b[], int off, int len) throws IOException {
        int total = 0;
        if (len == 0) {
            return 0;
        }

        int avail = intCount - intPos - 2;
        if (avail <= 0) {
            fill();
            avail = intCount - intPos - 2;
            if (avail <= 0) {
                return -1;
            }
        }
        int copy = Math.min(len, avail);
        System.arraycopy(byteBuffer, intPos, b, off, copy);
        intPos += copy;
        total += copy;
      
        while (total < len) {
            fill();
            avail = intCount - intPos - 2;
            if (avail <= 0) {
                return total;
            }
            copy = Math.min(len - total, avail);
            System.arraycopy(byteBuffer, intPos, b, off + total, copy);
            intPos += copy;
            total += copy;
        }
        return total;
    }

    /** InputStream から読むことができるバイト数をブロッキングなしで返します。
     * @return  (int)       バイト数
     * @throws  IOException (IOException)
     */
    public int available() throws IOException {
        int avail = (intCount - intPos - 2) + in.available();
        // Never return a negative value
        return (avail < 0 ? 0 : avail);
    }

    /** この InputStream を閉じます。
     * @throws  IOException (IOException)
     */
    public void close() throws IOException {
        if (!bEOF) {
            while (read(byteBuffer, 0, byteBuffer.length) != -1) {
                ; // 何もしない
            }
        }
    }
    
}
