/*
 * ParamPart.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.ByteArrayOutputStream;
import javax.servlet.ServletInputStream;

/** ParamPartクラス
 * 正常な<INPUT>(例えば、TYPE='file'では無い)形式パラメーターを表します。
 * @author  FFT corp.
 */
public class ParamPart extends Part {

    /** パラメータ値 */
    private byte[] byteValue;
    /** エンコーディング */
    private String strEncoding;

    /** ParamPartクラスコンストラクタ
     * @param   name            (String)キー
     * @param   in              (ServletInputStream)
     * @param   boundary        (String)
     * @param   encoding        (String)エンコーディング
     * @throws  IOException     (IOException)
     */
    public ParamPart(String name, ServletInputStream in, String boundary, String encoding) throws IOException {
        super(name);
        this.strEncoding = encoding;

        //バイト配列に内容をコピーする
        PartInputStream pis = new PartInputStream(in, boundary);
        ByteArrayOutputStream baos = new ByteArrayOutputStream(512);
        byte[] buf = new byte[128];
        int read;
        while ((read = pis.read(buf)) != -1) {
            baos.write(buf, 0, read);
        }
        pis.close();
        baos.close();
    
        //保存
        byteValue = baos.toByteArray();
    }

    /** パラメータ値をバイト配列で取得
     * @return  (byte[])    パラメータ値
     */
    public byte[] getValue() {
        return byteValue;
    }
    
    /** パラメータ値を文字列で取得
     * @return  (String)    パラメータ値
     * @throws  UnsupportedEncodingException     (UnsupportedEncodingException)
     */
    public String getStringValue() throws UnsupportedEncodingException {
        return getStringValue(strEncoding);
    }
  
    /** パラメータ値を文字列で取得
     * @param   encoding    (String)エンコーディング
     * @return  (String)    パラメータ値
     * @throws  UnsupportedEncodingException     (UnsupportedEncodingException)
     */
    public String getStringValue(String encoding) throws UnsupportedEncodingException {
        return new String(byteValue, encoding);
    }
  
    /** パラメータかどうか
     * @return  (boolean)   true = パラメータ、false = パラメータではない
     */
    public boolean isParam() {
        return true;
    }
    
}
