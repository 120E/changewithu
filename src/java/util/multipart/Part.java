/*
 * Part.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

/** Part抽象クラス
 * multipart/form-data形式の中で<INPUT>形式要素を表わす。
 * @author  FFT corp.
 */
public abstract class Part {
    
    /** 要素名 */
    private String strName;

    /** Partクラスコンストラクタ
     * @param   name    (String)    要素名
     */
    Part(String name) {
        this.strName = name;
    }
    
    /** 要素名取得
     * @return  (String)    要素名
     */
    public String getName() {
        return strName;
    }

    /** ファイルかどうか
     * @return  (boolean)   true = ファイル、false = ファイルではない
     */
    public boolean isFile() {
        return false;
    }

    /** パラメータかどうか
     * @return  (boolean)   true = パラメータ、false = パラメータではない
     */
    public boolean isParam() {
        return false;
    }
}
