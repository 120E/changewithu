/*
 * BufferedServletInputStream.java
 *
 * Created on 2004/04/21, 11:40
 */

package util.multipart;

import javax.servlet.ServletInputStream;
import java.io.IOException;
import javax.servlet.ReadListener;

/** ServletInputStreamを継承したBufferedServletInputStreamクラス
 * @author  FFT corp.
 */
public class BufferedServletInputStream extends ServletInputStream {

    /** ServletInputStreamクラス */
    private ServletInputStream sis;
    /** バッファ　64k */
    private byte[] byteBuffer = new byte[65536];
    /** バッファーへ読み込んだバイト数 */
    private int intCount; 
    /** バッファー中の現在の位置 */
    private int intPos;

    /** BufferedServletInputStreamクラスコンストラクタ
     * @param   in  (ServletInputStream)
     */
    public BufferedServletInputStream(ServletInputStream in) {
        this.sis = in;
    }

    /** ServletInputStreamからバッファーへ読込みます。
     * このメソッドを呼ぶ前に（バッファを上書きするため）、
     * バッファー中の文字がすべて使用されている事を保証しなければなりません。
     * @throws  IOException     (IOException)
     */
    private void fill() throws IOException {
        int i = sis.read(byteBuffer, 0, byteBuffer.length);
        if (i > 0) {
            intPos = 0;
            intCount = i;
        }
    }
    
    /** バッファリングを実装する、ラップされたServletInputStreamのreadLineメソッド
     * @param   b           (byte[])読込バッファ
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @return  (int)       -1 = EOF
     * @throws  IOException (IOException)
     */
    public int readLine(byte b[], int off, int len) throws IOException {
        int total = 0;
        if (len == 0) {
            return 0;
        }

        int avail = intCount - intPos;
        if (avail <= 0) {
            fill();
            avail = intCount - intPos;
            if (avail <= 0) {
                return -1;
            }
        }
        int copy = Math.min(len, avail);
        int eol = findeol(byteBuffer, intPos, copy);
        if (eol != -1) {
            copy = eol;
        }
        System.arraycopy(byteBuffer, intPos, b, off, copy);
        intPos += copy;
        total += copy;

        while (total < len && eol == -1) {
            fill();
            avail = intCount - intPos;
            if (avail <= 0) {
                return total;
            }
            copy = Math.min(len - total, avail);
            eol = findeol(byteBuffer, intPos, copy);
            if (eol != -1) {
                copy = eol;
            }
            System.arraycopy(byteBuffer, intPos, b, off + total, copy);
            intPos += copy;
            total += copy;
        }
        return total;
    }

    /** ServletInputStreamのreadLineメソッドのコメントに定義されるような、
     * ライン・マーカーの終了 (\n) を見つけることを試みる。
     * @param   b       サーチするバイト配列
     * @param   pos     サーチを開始するバイト配列中の位置
     * @param   len     バイト配列の長さ
     * @return  (int)   サーチを開始してから見つかったバイト数、又は見つからない場合、-1
     */
    private static int findeol(byte b[], int pos, int len) {
        int end = pos + len;
        int i = pos;
        while (i < end) {
            if (b[i++] == '\n') {
                return i - pos;
            }
        }
        return -1;
    }
  
    /** バッファリングを実装する、ラップされたServletInputStreamのreadメソッド
     * @return  (int)       次のバイトデータ、又は -1 (ストリームの終了)
     * @throws  IOException (IOException)
     */
    public int read() throws IOException {
        if (intCount <= intPos) {
            fill();
            if (intCount <= intPos) {
                return -1;
            }
        }
        return byteBuffer[intPos++] & 0xff;
    }

    /** バッファリングを実装する、ラップされたServletInputStreamのreadメソッド
     * @param   b           (byte[])読込バッファ
     * @param   off         (int)オフセット
     * @param   len         (int)バッファの長さ
     * @return  (int)       読込バイト数、又はストリームが終了している場合 -1
     * @throws  IOException (IOException)
     */
    public int read(byte b[], int off, int len) throws IOException {
        int total = 0;
        while (total < len) {
            int avail = intCount - intPos;
            if (avail <= 0) {
                fill();
                avail = intCount - intPos;
                if (avail <= 0) {
                    if (total > 0) {
                        return total;
                    } else {
                        return -1;
                    }
                }
            }
            int copy = Math.min(len - total, avail);
            System.arraycopy(byteBuffer, intPos, b, off + total, copy);
            intPos += copy;
            total += copy;
        }
        return total;
    }

    @Override
    public boolean isFinished() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean isReady() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setReadListener(ReadListener rl) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
