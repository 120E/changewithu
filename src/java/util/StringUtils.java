package util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;

public class StringUtils extends org.apache.commons.lang.StringUtils {
    

    public StringUtils (){
    }
    
    
    /**
     * テスト用
     */
    public static void main (String [] args) {
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 3) is  \'" + abbreviate2BytesSafe ("１２345６７890", 3) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 4) is  \'" + abbreviate2BytesSafe ("１２345６７890", 4) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 5) is  \'" + abbreviate2BytesSafe ("１２345６７890", 5) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 6) is  \'" + abbreviate2BytesSafe ("１２345６７890", 6) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 7) is  \'" + abbreviate2BytesSafe ("１２345６７890", 7) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 8) is  \'" + abbreviate2BytesSafe ("１２345６７890", 8) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 9) is  \'" + abbreviate2BytesSafe ("１２345６７890", 9) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 10) is \'" + abbreviate2BytesSafe ("１２345６７890", 10) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 11) is \'" + abbreviate2BytesSafe ("１２345６７890", 11) + "\'");
        System.out.println ("abbreviate2BytesSafe (\"１２345６７890\", 12) is \'" + abbreviate2BytesSafe ("１２345６７890", 12) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 3) is  \'" + abbreviate3BytesSafe ("１２345６７890", 3) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 4) is  \'" + abbreviate3BytesSafe ("１２345６７890", 4) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 5) is  \'" + abbreviate3BytesSafe ("１２345６７890", 5) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 6) is  \'" + abbreviate3BytesSafe ("１２345６７890", 6) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 7) is  \'" + abbreviate3BytesSafe ("１２345６７890", 7) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 8) is  \'" + abbreviate3BytesSafe ("１２345６７890", 8) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 9) is  \'" + abbreviate3BytesSafe ("１２345６７890", 9) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 10) is \'" + abbreviate3BytesSafe ("１２345６７890", 10) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 11) is \'" + abbreviate3BytesSafe ("１２345６７890", 11) + "\'");
        System.out.println ("abbreviate3BytesSafe (\"１２345６７890\", 12) is \'" + abbreviate3BytesSafe ("１２345６７890", 12) + "\'");
    }

    
    
    
    /**
     * 文字列をハッシュ化（不可逆）
     * @param orgString
     * @return hashString
     */
    public static String getMD5(String orgString){
        String hashString = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(orgString.getBytes());
            byte[] hashBytes = md.digest();
            int[] hashInts = new int[hashBytes.length];
            StringBuilder sb = new StringBuilder();
            for (int i=0; i < hashBytes.length; i++) {
                hashInts[i] = (int)hashBytes[i] & 0xff;
                if (hashInts[i] <= 15) {
                    sb.append("0");
                }
                sb.append(Integer.toHexString(hashInts[i]));
            }
            
            hashString = sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(StringUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hashString;
    }
    
    
    /**
     * URLパラメータの作成
     * @param name部
     * @param value部
     * @return "&name=value"
     */
    public static String getUrlParam (String name, String value) {
        return "&" + name + "=" + value;
    }
    
    /**
     * URLパラメータの作成
     * @param name部
     * @param value部
     * @return "&name=value"
     */
    public static String getUrlParam (String name, boolean value) {
        return "&" + name + "=" + value;
    }
    
    /**
     * URLパラメータの作成
     * @param name部
     * @param value部
     * @return "&name=value"
     */
    public static String getUrlParam (String name, int value) {
        return "&" + name + "=" + value;
    }
    
    /**
     * URLパラメータの作成
     * @param name部
     * @param value部
     * @return "&name=value"
     */
    public static String getUrlParam (String name, long value) {
        return "&" + name + "=" + value;
    }
    
    

    /**
     * isValidMail
     * メールアドレスを表す文字列かどうか
     */
    public static boolean isValidMail (String inString) {
        int atMarkPosition = inString.indexOf ('@');
        if ((atMarkPosition <= 2 ) || (atMarkPosition != inString.lastIndexOf ('@')) || (atMarkPosition == inString.length () - 1))
            return false;
        int dotPosition = inString.indexOf ('.');
        if ((dotPosition <= 1 ) || (dotPosition == inString.length () - 1))
            return false;
        return isValidString (
                inString
                ,"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.@!#$%&'*+-/=?^_`{|}~"
                );
    }
    
    
    
    /**
     * isValidId
     * Id,Passwordで使用可能な文字列かどうか
     */
    public static boolean isValidId (String inString) {
        String charBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_@";
        char inString_Array [] = inString.toCharArray ();
        char charBase_Array [] = charBase.toCharArray ();
        boolean stringEqual = true;
        for (int i = 0; i < inString_Array.length; i++) {
            boolean charEqual = false;
            for (int j = 0; j < charBase_Array.length; j++) {
                if (inString_Array [i] == charBase_Array [j]) {
                    charEqual = true;
                    break;
                }
            }
            if (!charEqual) {
                stringEqual = false;
                break;
            }
        }
        return stringEqual;
    }   


    /**
     * isValidIpaddress
     * Ipaddressを表す文字列かどうか
     */
    public static boolean isValidIpaddress (String inString) {
        boolean noError = true;

        java.util.StringTokenizer st = new java.util.StringTokenizer (inString, ".");
        if (st.countTokens() != 4) {
            noError = false;
        }else{
            String charBase = "0123456789";
            char charBase_Array [] = charBase.toCharArray ();
            while (st.hasMoreTokens ()) {
                char inString_Array [] = st.nextToken ().toCharArray ();
                boolean stringEqual = true;
                for (int i = 0; i < inString_Array.length; i++) {
                    boolean charEqual = false;
                    for (int j = 0; j < charBase_Array.length; j++) {
                        if (inString_Array [i] == charBase_Array [j]) {
                            charEqual = true;
                            break;
                        }
                    }
                    if (!charEqual) {
                        stringEqual = false;
                        break;
                    }
                }
                if (!stringEqual) {
                    noError = false;
                    break;
                }
            }
        }
        return noError;
    }
    
    
    /**
     * isValidUrl
     * URL文字列の検証
     */
    public static boolean isValidUrl (String inString) {
        String charBase = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_:;//?&=~%@#";
        char inString_Array [] = inString.toCharArray ();
        char charBase_Array [] = charBase.toCharArray ();
        boolean stringEqual = true;
        for (int i = 0; i < inString_Array.length; i++) {
            boolean charEqual = false;
            for (int j = 0; j < charBase_Array.length; j++) {
                if (inString_Array [i] == charBase_Array [j]) {
                    charEqual = true;
                    break;
                }
            }
            if (!charEqual) {
                stringEqual = false;
                break;
            }
        }
        return stringEqual;
    }   


    /**
     * isValidString
     * charBaseに含まれる文字のみで構成されているかチェック
     * 数字の場合は、charBaseに"0123456789"
     * 英字の場合は、charBaseに"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
     */
    public static boolean isValidString (String inString, String charBase) {
        char inString_Array [] = inString.toCharArray ();
        char charBase_Array [] = charBase.toCharArray ();
        boolean stringEqual = true;
        for (int i = 0; i < inString_Array.length; i++) {
            boolean charEqual = false;
            for (int j = 0; j < charBase_Array.length; j++) {
                if (inString_Array [i] == charBase_Array [j]) {
                    charEqual = true;
                    break;
                }
            }
            if (!charEqual) {
                stringEqual = false;
                break;
            }
        }
        return stringEqual;
    }   


    /**
     * 文字列を指定バイト長の省略<br>
     * abbreviate2BytesSafe("abcdefghij", 9)  --> "abcdef..."<br>
     * abbreviate2BytesSafe("abcdefghij", 10) --> "abcdefghij"<br>
     * abbreviate2BytesSafe("あいうえおかきくけこ", 9) --> "あいう..."<br>
     * abbreviate2BytesSafe("あいうえおかきくけこ", 10) --> "あいう ..."<br>
     * @param s 変換対象文字列
     * @param l 出力バイト長
     * @return 省略文字列
     */
    public static String abbreviate2BytesSafe (String s, int l) {
        int endPoint = 0;
        boolean supply1Byte = false;
        try {
            if (s.getBytes ().length <= l) {
                return s;
            } else {
                if (l <= 3) {
                    return ("...");
                }
            }
            for (int i = 0 ; i < s.length (); i ++) {
                //System.out.println (i + "," + s.substring (0, i + 1).getBytes ().length);
                if (s.substring (0, i + 1).getBytes ().length >= l - 3) {
                    if (s.substring (0, i + 1).getBytes ().length == l - 3) {
                        endPoint = i;
                    } else {
                        endPoint = i - 1;
                        supply1Byte = true;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace (System.out);
        }
        return s.substring (0, endPoint + 1) + (supply1Byte ? " " : "") + "...";
    }
    
    
    
    
    /**
     * 文字列を指定バイト長の省略<br>
     * abbreviate2BytesSafe("abcdefghij", 9)  --> "abcdef..."<br>
     * abbreviate2BytesSafe("abcdefghij", 10) --> "abcdefghij"<br>
     * abbreviate2BytesSafe("あいうえおかきくけこ", 9) --> "あい..."<br>
     * abbreviate2BytesSafe("あいうえおかきくけこ", 10) --> "あい ..."<br>
     * @param s 変換対象文字列
     * @param l 出力バイト長
     * @return 省略文字列
     */
    public static String abbreviate3BytesSafe (String s, int l) {
        int endPoint = 0;
        boolean supply1Byte = false;
        try {
            if (s.getBytes ("utf-8").length <= l) {
                return s;
            } else {
                if (l <= 3) {
                    return ("...");
                }
            }
            for (int i = 0 ; i < s.length (); i ++) {
                //System.out.println (i + "," + s.substring (0, i + 1).getBytes ().length);
                if (s.substring (0, i + 1).getBytes ("utf-8").length >= l - 3) {
                    if (s.substring (0, i + 1).getBytes ("utf-8").length == l - 3) {
                        endPoint = i;
                    } else {
                        endPoint = i - 1;
                        supply1Byte = true;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace (System.out);
        }
        return s.substring (0, endPoint + 1) + (supply1Byte ? " " : "") + "...";
    }
    
    
    
    
    public static String JISToCP932 (String s) {
        //REVERSE SOLIDUS, WAVE DASH, DOUBLE VERTICAL LINE, MINUS SIGN, CENT SIGN, POUND SIGN, NOT SIGN
        char [] from = {0x005c, 0x301c, 0x2016, 0x2212, 0x00a2, 0x00a3, 0x00ac};
        //FULLWIDTH REVERSE SOLIDUS, FULLWIDTH TILDE, PARALLEL TO, FULLWIDTH HYPHEN-MINUS, FULLWIDTH CENT SIGN, FULLWIDTH POUND SIGN, FULLWIDTH NOT SIGN
        char [] to = {0xff3c, 0xff5e, 0x2225, 0xff0d, 0xffe0, 0xffe1, 0xffe2};
        StringBuffer sb = new StringBuffer ();
        char c;
        for (int j = 0; j < s.length (); j ++) {
            c = s.charAt (j);
            for (int k = 0; k < from.length; k ++) {
                if (c == from [k]) {
                    c = to [k];
                    break;
                }
            }
            sb.append (c);
        }
        return new String (sb);
    } 
    public static String CP932toJIS (String s) {
        //FULLWIDTH REVERSE SOLIDUS, FULLWIDTH TILDE, PARALLEL TO, FULLWIDTH HYPHEN-MINUS, FULLWIDTH CENT SIGN, FULLWIDTH POUND SIGN, FULLWIDTH NOT SIGN
        char [] from = {0xff3c, 0xff5e, 0x2225, 0xff0d, 0xffe0, 0xffe1, 0xffe2};
        //REVERSE SOLIDUS, WAVE DASH, DOUBLE VERTICAL LINE, MINUS SIGN, CENT SIGN, POUND SIGN, NOT SIGN
        char [] to = {0x005c, 0x301c, 0x2016, 0x2212, 0x00a2, 0x00a3, 0x00ac};
        StringBuffer sb = new StringBuffer ();
        char c;
        for (int j = 0; j < s.length (); j ++) {
            c = s.charAt (j);
            for (int k = 0; k < from.length; k ++) {
                if (c == from [k]) {
                    c = to [k];
                    break;
                }
            }
            sb.append (c);
        }
        return new String (sb);
    } 
    public static String UTF8toSJIS (String s) {
        s = s.replaceAll("➖", "-");
        char [] from = {0x301c, 0x2016, 0x2212, 0x00a2, 0x00a3, 0x00ac, 0x2014};
        char [] to = {0xff5e, 0x2225, 0xff0d, 0xffe0, 0xffe1, 0xffe2, 0x2015};
        StringBuffer sb = new StringBuffer ();
        char c;
        for (int j = 0; j < s.length (); j ++) {
            c = s.charAt (j);
            for (int k = 0; k < from.length; k ++) {
                if (c == from [k]) {
                    c = to [k];
                    break;
                }
            }
            sb.append (c);
        }
        return new String (sb);
    } 

    
    
    /**
     * 配列を連結（String）
     * to({"1","2","3"},",") = "\"1\",\"2\",\"3\"";
     * @param ary String array
     * @param delim delimiter
     * @return String
     */
    public static String join(String[] ary, String delim){
        String result="";
        if(ary!=null)
        for(int n=0; n<ary.length; n++)
            result += (result.length()==0?"":delim) +"\""+ ary[n] +"\"";
        return result;
    }
    
    /**
     * 配列を連結（int）
     * to({1,2,3},",") = "1,2,3";
     * @param ary int array
     * @param delim delimiter
     * @return String
     */
    public static String join(int[] ary, String delim){
        String result="";
        if(ary!=null)
        for(int n=0; n<ary.length; n++)
            result += (result.length()==0?"":delim) +ary[n];
        return result;
    }
    
    
    /**
     * 数字文字列に変換
     */
    public static String toDigit(String value){
        String result="";
        char[] ch = value.toCharArray();
        for(int n=0; n<ch.length; n++)
            if(Character.isDigit(ch[n]))
                result+=Character.getNumericValue(ch[n]);
        return result;
    }

    public static String verifyNotEmpty(String str, String defaultValue) {
        return isNotEmpty(str) ? str : defaultValue;
    }
    
    
    /**
     * 受付番号を生成
     * @return 
     */
    public static String makeAcceptNumber(){
        String result = "";
        char[] alphabet = "qwertyupasdfghjkzxcvbnm".toCharArray();
        result += alphabet[(new java.util.Random()).nextInt(alphabet.length)];
        result += RandomStringUtils.randomNumeric(7);
        return result;
    }
    
    
    
    /**
     * isFullKana
     * 全角カナの検証
     */
    public static boolean isFullKana(String str) {
		return Pattern.matches("^[ァ-ヶー　]*$", str);
	}
    
    /**
     * isHalfNum
     * 半角数字の検証
     */
    public static boolean isHalfNum(String str) {
		return Pattern.matches(("^[0-9]*$"), str);
	}
    
    
}
