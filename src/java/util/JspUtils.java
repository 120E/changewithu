/*
 * JspUtils.java
 *
 * Created on 2003/12/05, 18:00
 */

package util;

import javax.servlet.http.HttpServletRequest;
import org.apache.tomcat.util.buf.UDecoder;

/**
 * JSP関連ユーティリティクラス
 * @author FFT corp.
 */
public class JspUtils extends javax.servlet.http.HttpServlet {
    
    /**
     * JSP表示処理
     * @param request servlet request
     * @param response servlet response
     * @param jspPath jspファイルパス 
     * @throws javax.servlet.ServletException Servlet例外
     * @throws java.io.IOException IO例外
     */
    public static void sendToJsp (javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, String jspPath)
    throws javax.servlet.ServletException, java.io.IOException {
        
        javax.servlet.RequestDispatcher rd = request.getRequestDispatcher (jspPath);
        rd.forward (request, response);
    }
    
    
    
    /**
     * スマホ判定（タブレットは含まない）
     * @param request
     * @return 
     */
    public static boolean isSmartPhone(javax.servlet.http.HttpServletRequest request){
        String ua = request.getHeader("User-Agent");
        System.out.println(ua);
        if(ua.indexOf("iPhone")!=-1)
            return true;
        else if(ua.indexOf("iPod")!=-1)
            return true;
        else if(ua.indexOf("Android")!=-1 && ua.indexOf("Mobile")!=-1)
            return true;
        else
            return false;
    }
    
    
    /*
    public static boolean checkMode(HttpServletRequest request){
        boolean _isNormalMode = true;
        db.AdmAccount dbAdmAccountSession = (db.AdmAccount)request.getAttribute("dbAdmAccountSession");
        String _mode = (String)request.getAttribute("MODE");
        String _modePath = (String)request.getAttribute("MODE_PATH");
        boolean _isValidPath = dbAdmAccountSession!=null && dbAdmAccountSession.getSite()!=0;
        if(!_isValidPath){
            if(_modePath!=null && (request.getServletPath()+"?"+request.getQueryString()).startsWith(UDecoder.URLDecode(_modePath)))
                _isValidPath = true;
        }
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>_modePath:"+UDecoder.URLDecode(_modePath));
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>_modePath:"+((request.getServletPath()+"?"+request.getQueryString()).startsWith(UDecoder.URLDecode(_modePath))));
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>getQueryString:"+request.getQueryString());
//        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>getServletPath:"+request.getServletPath());
        
        
//        if(dbAdmAccountSession!=null && dbAdmAccountSession.getFlagSuperuser()==1 && _mode!=null && _mode.equals("preview")){
//            //プレビュー（特権のみ）
        if(dbAdmAccountSession!=null && _mode!=null && _mode.equals("preview") && _isValidPath){
            //プレビュー（PATH一致のみ（特権管理者は'/'なので全OKのはず））
            _isNormalMode = false;
        }else{
            //通常
            request.setAttribute("MODE", "normal");
        }
        return _isNormalMode;
    }
    */

    

}
