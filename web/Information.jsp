<%@page import="util.StringEscapeUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
        <head>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KRHCSM8');</script>
            <!-- End Google Tag Manager -->
            <title>おしらせ | チェンジをともに</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
            <meta name="description" content="「チェンジをともに（チェンとも）」は理想の出会いをカタチにする結婚相談所です。あなたのご結婚までトレーナー・カウンセラーが全力でサポートいたします。">
            <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
            <script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
            <script src="js/common.js" type="text/javascript"></script>
            <link rel="stylesheet" media="all" href="css/common.css">
            <link rel="stylesheet" media="screen and (max-width: 767px)" href="css/common_sp.css">
        </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTNL6SC"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        
        <div class="base_contents">
            <header>
                <div class="change_logo">
                    <a href="index.html"><img src="images/change_logo.png" alt=""/></a>
                </div>
                <div class="right_menu">
                    <a class="right_menu_block menu_consultation" target="blank"  href="https://forms.gle/mS2hvpg3a72Vu4oq6">無料相談予約</a>
                </div>
                <nav class="menu">
                    <ul>
                        <li><a href="index.html#prices">コース・料金</a></li>
                        <li><a href="index.html#safety">安心/安全な出会い</a></li>
                        <li><a href="index.html#activity">活動開始の流れ</a></li>
                        <li><a href="policy.html">サイトポリシー</a></li>
                    </ul>
                </nav>
            </header>
            <div class="main_contents">
                <div class="block_contents">
                    <div class="content_title">
                        <h1>おしらせ</h1>
                    </div>
                    <div class="infoPage">
                        <%
                        ArrayList<db.Information> dbInformation = (ArrayList<db.Information>)request.getAttribute("dbInformation");
                        if(dbInformation==null || dbInformation.isEmpty()){ %>
                            現在、おしらせはありません<%
                        }else{ %>
                            <dl><%
                                for(db.Information _info : dbInformation){ %>
                                    <dt>
                                        <span class="date"><%=esc(_info.getDispDate())%></span>
                                        <%=esc(_info.getTitle())%>
                                    </dt><%
                                    if(0<_info.getContent().length()){ %>
                                        <dd>
                                            <%=escBR(_info.getContent())%>
                                        </dd><%
                                    }//if
                                }//for
                                %>
                            </dl><%
                        }//else
                        %>
                    </div><!-- infoPage -->
                </div>
                
            </div>
            <footer class="site_footer">
                <div class="page_top">
                    <a href="#"><img src="images/page_top.png" alt=""/></a>
                </div>
                <h2>チェンジをともに</h2>
                <div class="footer_">
                    <div class="footer_address">
                        <p>TEL：080-6285-6285</p>
                        <p>TEL：<a class="tel" href="tel:080-6285-6285">080-6285-6285</a></p>
                        <p>MAIL：<a class="mail" href="mailto:change.with.u.kochi@gmail.com">change.with.u.kochi@gmail.com</a></p>
                    </div>
                    <nav class="footer_navi">
                        <ul>
                            <li><a href="policy.html">サイトポリシー</a></li>
                            <li><a target="blank" href="https://www.biu.jp/">日本ブライダル連盟</a></li>
                            <li><a href="#">Facebook</a></li>
                        </ul>
                    </nav>
                </div>
            </footer>
            <div class="copyright">
                Copyright ©  2020 チェンジをともに All Rights Reserved.
            </div>  
        </div><!-- base_contents -->
    </body>
</html>
<%!
    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>