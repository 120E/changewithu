<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="util.StringEscapeUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WTNL6SC');</script>
        <!-- End Google Tag Manager -->
        <title>天河ボクシングジム（てんがボクシングジム）</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="天河 ボクシングジム （てんが ボクシングジム） は、小学生からシニアまで男女を問わずトレーニングを楽しめる、高知のボクシングジムです。現在、練習生募集中です！本格的にボクシングに取り組みたい方だけでなく、健康目的の方も多数在籍しています。女性向けのボクササイズ・フィットネスメニューも用意しています。">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <link rel="stylesheet" media="all" href="css/common.css?=20200611a">
        <link rel="stylesheet" media="screen and (max-width: 767px)" href="css/common_sp.css?=20200611a">
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTNL6SC"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        
        <div class="page_description">
            天河ボクシングジム（てんが ボクシングジム）は、小学生からシニアまで男女を問わずトレーニングを楽しめる、高知のボクシングジムです。
            現在、練習生募集中です！本格的にボクシングに取り組みたい方だけでなく、健康目的の方も多数在籍しています。女性向けのボクササイズ・フィットネスメニューも用意しています。
        </div>

        <div class="photo_main">
            <img src="images/photo_main.png">
        </div>
        
        <div class="base_contents">
            
            
            
            <header>
                <ul class="menu">
                    <li><a href="#prices">料金</a></li>
                    <li><a href="#calendar">営業カレンダー</a></li>
                    <li><a href="#faq">よくあるご質問</a></li>
                    <li><a href="#access">アクセス</a></li>
                    <li><a href="policy.html">サイトポリシー</a></li>
                    <li>
                        <a href="#"><img src="images/logo_w.png" class="logo_s"></a>
                    </li>
                </ul>
                <div class="contacts">
                    <a href="tel:088-855-4774"><img src="images/phone.png"></a>
                    <a href="mailto:tengaboxinggym@gmail.com"><img src="images/mail.png"></a>
                </div>
                <a href="javascript:void(0);" class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </header>
            
            
            
            
            <section class="info">
                <span class="label">
                    おしらせ
                </span>
                <a href="information.html" class="article"><%
                    ArrayList<db.Information> dbInformation = (ArrayList<db.Information>)request.getAttribute("dbInformation");
                    if(dbInformation==null || dbInformation.isEmpty()){ %>
                        現在、おしらせはありません<%
                    }else{ %>
                        <%=esc(dbInformation.get(0).getTitle())%><%
                        if(dbInformation.get(0).isNew()) { %>
                            <span>new</span><%
                        }
                    } %>
                </a>
            </section>
            
            
            
            <img src="images/logo_s.png" class="logo_s">
            <img src="images/catch_main.png" class="catch_main">

            
            
            
<!--            <div class="training">
                <a href="#">
                    <img src="images/points_photo_1.jpg">
                    <span>5/27 練習の様子（マスボクシング）</span>
                </a>
            </div>-->



            <div class="three_points">
                <ul>
                    <li>
                        <img class="photo" src="images/points_photo_1.jpg">
                        <img class="label" src="images/points_label_1.png">
                        <span>
                            マス・ボクシング※を中心としたメニューで、選手でなくても、より実践に近い緊張感を持ってトレーニングができます。
                        </span>
                        <span class="mass_boxing">
                            ※マス・ボクシングは、力を入れずに打ち合うスパーリングです。
                        </span>
                    </li>
                    <li>
                        <img class="photo" src="images/points_photo_2.jpg">
                        <img class="label" src="images/points_label_2.png">
                        <span>
                            小学生～シニアまで、男女を問わず幅広い練習生がいます。トレーニング仲間との新たな和を作ってみませんか？
                        </span>
                    </li>
                    <li>
                        <img class="photo" src="images/points_photo_3.jpg">
                        <img class="label" src="images/points_label_3.png">
                        <span>
                            数日のトレーニングで、すぐに上達を実感できます。楽しみながら継続すれば、新しい自分に出会えます。
                        </span>
                    </li>
                </ul>
                <!--<span class="mass_boxing">※マス・ボクシングは、力を入れずに打ち合うスパーリングです。</span>-->
            </div><!-- three_points -->
            
            
            
            <a class="btn_entry_1" target="_blank" href="https://forms.gle/zyF1JV5yqo91ER3d8"><img src="images/btn_entry_1.png"></a>
            
            
            
            <div class="ladies">
                <img src="images/ladies_photo.jpg">
                <a class="btn_entry_2" target="_blank" href="https://forms.gle/zyF1JV5yqo91ER3d8"><img src="images/btn_entry_2.png"></a>
            </div>
            
            
            
            
            <div class="about_entry">
                <a class="banner" target="_blank" href="nocount.html"><img src="images/banner_nocount_1.jpg" alt="ダイエット健康サプリ「NO-Cont（ノーカウント）」"></a>
                
                
                <div class="bigtitle_1">見学・体験・入会について</div>
                <table>
                    <tr>
                        <td>
                            <div class="title">見学</div>
                            見学は事前のご予約は不要です。<br>お気軽にお越しのうえ、『見学希望』の旨をお伝え下さい。
                        </td>
                        <td>
                            <div class="title">体験</div>
                            専用のトレーニングメニューを体験していただきます。<br>
                            体験申込フォームまたはお電話にて、ご予約ください。<br>
                            ※体験料は、1,200円（一週間以内に入会すればキャッシュバック）です。
                        </td>
                        <td>
                            <div class="title">入会</div>
                            <span class="red">入会の前に体験が必要です。</span><br>
                            <入会に必要なもの><br>
                            ・入会申込書
                            <span class="small">（18歳未満の方は、保護者の承諾が必要です。）</span>
                            ・身分証明書
                            <span class="small">（免許証、保険証、学生証等）</span>
                            ・入会費・会費（現金のみ）
                        </td>
                    </tr>
                </table>
                <div class="bigtitle_2">見学・体験・入会についての同意事項</div>
                <div class="description_2">
                    ※暴力団や反社会的勢力との関係が無いこと。<br>
                    ※医師等により運動を禁じられていないこと。また妊娠や、他人に伝染する恐れのある疾病を有していないこと。<br>
                    ※ジムおよび従業員・会員に対する迷惑行為、名誉・品位を害する行為をしないこと。またジム内における宗教活動・営業行為、秩序を乱す行為をしないこと。
                </div>
            </div>
            
            
            
            
            <div id="prices" class="prices">
                <div class="bigtitle">料金プラン</div>
                <table>
                    <tr>
                        <th>体験料</th>
                        <td>
                            <strong>1,200</strong>円
                            →
                           キャッシュバックで実質<strong class="red">0</strong>円
                           <span class="red small">※</span>
                        </td>
                    </tr>
                </table>
                <div class="attention">
                    ※一週間以内に入会した場合のみ。
                </div>
                <table>
                    <tr>
                        <th>&nbsp;</th>
                        <th>社会人</th>
                        <th>学生</th>
                        <th>小学生<span class="small">（小4～）</span></th>
                    </tr>
                    <tr>
                        <td>入会料</td>
                        <td colspan="3"><strong>5,500</strong>円</td>
                    </tr>
                    <tr>
                        <td>月会費</td>
                        <td><strong>8,800</strong>円</td>
                        <td><strong>7,700</strong>円</td>
                        <td><strong>6,500</strong>円</td>
                    </tr>
                        <td>
                            チケット
                            <span class="small red">※</span>
                        </td>
                        <td colspan="3">
                            <strong>12,000</strong>円
                            （<strong>10</strong>回分）
                        </td>
                    </tr>
                </table>
                <div class="attention">
                    ※月毎に会費（月額）払い／チケットを選択できて便利！<br>
                    ※数日しかトレーニングに来られない月には、是非チケットをご利用ください。<br>
                    ※有効期限は6ヶ月ですので、ご注意ください。
                </div>
                <table>
                    <tr>
                        <th>利用可能時間帯</th>
                        <td>
                            <div class="weekday">
                                <div><strong>月火水<s>木</s>金<s>土</s></strong></div>
                                <div><strong><s>月火水</s>木<s>金土</s></strong></div>
                                <div><strong class="blue"><s>月火水木金</s>土</strong></div>
                                <div><strong class="red">日・祝</strong></div>
                            </div>
                            <div class="times">
                                <div><strong>15</strong>時～<strong>22</strong>時</div>
                                <div><strong>17</strong>時～<strong>21</strong>時</div>
                                <div><strong>15</strong>時～<strong>21</strong>時</div>
                                <div><strong>休</strong>業日</div>
                            </div>
                        </td>
                    </tr>
                </table>
                <div class="attention">
                    ※毎月臨時休業日があります。営業カレンダーでご確認ください。
                </div>
            </div>
            
            
            
            
            
            <div id="calendar" class="calendar">
                <div class="bigtitle">営業カレンダー</div>
                <div class="customCalendar"><%
                    ArrayList<db.ServiceCalendar> dbServiceCalendarCurrent = (ArrayList<db.ServiceCalendar>)request.getAttribute("dbServiceCalendarCurrent");
                    ArrayList<db.ServiceCalendar> dbServiceCalendarNext = (ArrayList<db.ServiceCalendar>)request.getAttribute("dbServiceCalendarNext");
                    ArrayList<db.PublicHolidays> dbPublicHolidaysCurrent = (ArrayList<db.PublicHolidays>)request.getAttribute("dbPublicHolidaysCurrent");
                    ArrayList<db.PublicHolidays> dbPublicHolidaysNext = (ArrayList<db.PublicHolidays>)request.getAttribute("dbPublicHolidaysNext");
                    //カレンダー2ヶ月表示
                    //getCalendarで1月分を構成
                    try{
                        Calendar _cal = Calendar.getInstance(Locale.JAPANESE);
                        out.println(getCalendar(dbServiceCalendarCurrent, dbPublicHolidaysCurrent, _cal));
                        _cal.set(Calendar.MONTH, _cal.get(Calendar.MONTH)+1);
                        out.println(getCalendar(dbServiceCalendarNext, dbPublicHolidaysNext, _cal));
                    }catch(Exception e){
                        e.printStackTrace(System.out);
                    }
                    %>
                </div>
                <!--<div class="googleCalender">
                    <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23039BE5&amp;ctz=Asia%2FTokyo&amp;src=dGVuZ2EuYm94aW5nLmd5bS40Nzc0QGdtYWlsLmNvbQ&amp;src=cmxqZmJmaG9kamczYjg4YjQxamVjOTU5bDRAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;src=amEuamFwYW5lc2UjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%23039BE5&amp;color=%23D50000&amp;color=%230B8043&amp;showTitle=0&amp;showNav=0&amp;showPrint=0&amp;showTabs=0&amp;showTz=0" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                </div>-->
            </div>
            


            
            <a class="btn_entry_3" target="_blank" href="https://forms.gle/zyF1JV5yqo91ER3d8"><img src="images/btn_entry_3.png"></a>



            
            <div id="faq" class="faq">
                <div class="bigtitle">よくあるご質問</div>
                <div class="q">Q. 見学や体験はできますか？</div>
                <div class="a">
                    見学は無料です。営業時間内であればいつでも大丈夫です。<br>
                    体験は有料です。<a target="_blank" href="https://forms.gle/zyF1JV5yqo91ER3d8">体験申し込みフォーム</a>またはお電話（<a href="tel:088-855-4774">088-855-4774</a>）へ事前にご予約ください。体験時間は60分程度です。
                </div>
                <div class="q">Q. 体験時は何が必要ですか？</div>
                <div class="a">
                    体験料 1,200円、動きやすい服装、室内履きシューズ、タオル、飲み物（ジムに自動販売機もあります）
                </div>
                <div class="q">Q. ボクシング未経験者でも大丈夫ですか？</div>
                <div class="a">
                    当ジム会員の大半は未経験者です。社会人・シニアの入会も多く、初心者の方にも親切・丁寧に指導いたします。<br>
                    もちろん経験者・選手のご入会も大歓迎です。
                </div>
                <div class="q">Q. 更衣室やシャワー、ロッカーはありますか？</div>
                <div class="a">
                    男女それぞれの更衣室に、ロッカー及びシャワールームを用意しています。
                </div>
                <div class="q">Q. ダイエット目的でも入会できますか？</div>
                <div class="a">
                    ダイエット目的でも、体力に自信がなくても大丈夫です。初めての方でも、無理のないよう基礎からトレーニングしていただきます。
                </div>
                <div class="q">Q. 女性でも入会できますか？</div>
                <div class="a">
                    女性の為のレディーストレーニングメニューもご用意しています。
                </div>
                <div class="q">Q. 年齢制限はありますか？</div>
                <div class="a">
                    小学生（4年生）から受け付けています。<br>
                    中高年の方も楽しみながら練習が出来ます。
                </div>
                <div class="q">Q. 駐車場はありますか？</div>
                <div class="a">
                    ジムの駐車場には数台のスペースがありますが、いっぱいの場合は近隣のコインパーキング等をご利用ください。
                </div>


                <a class="banner" target="_blank" href="nocount.html"><img src="images/banner_nocount_2.jpg" alt="ダイエット健康サプリ「NO-Cont（ノーカウント）」"></a>


            </div>





            <div id="access" class="access">
                <div class="bigtitle">アクセス</div>
                <div class="googleMap">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3324.847167641495!2d133.57255231529655!3d33.557346980743525!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x354e1c0956b2d50b%3A0x7f664f0f00614ce5!2z5aSp5rKz44Oc44Kv44K344Oz44Kw44K444Og!5e0!3m2!1sja!2sjp!4v1590582492563!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div class="contact">
                    <span>天河ボクシングジム</span>
                    <span>〒781-8121 高知県高知市葛島2丁目7-21</span>
                    <span>088-855-4774</span>
                </div>
            </div>



            
            <div class="instagram">
                <a target="_blank" href="https://www.instagram.com/tengaboxinggym/?hl=ja"><img src="images/instagram.png"></a>
                <div class="widget">
                    <!-- SnapWidget -->
                    <script src="https://snapwidget.com/js/snapwidget.js"></script>
                    <iframe src="https://snapwidget.com/embed/832676" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>
                </div>
            </div>
            
            
            
            <div class="copyright">
                Copyright&copy; 天河ボクシングジム　All rights reserved.
            </div>


            
            <a href="#" class="goTop"></a>

            
            
        </div><!-- base_contents -->
    </body>
</html>
<%!
    /**
     * カレンダー表示部
     */
    private String getCalendar(ArrayList<db.ServiceCalendar> dbServiceCalendar, ArrayList<db.PublicHolidays> dbPublicHolidays, Calendar _cal){

        StringBuffer _sb = new StringBuffer();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
        _cal.set(Calendar.DAY_OF_MONTH, 1);
        int _firstDayOfWeek = _cal.get(Calendar.DAY_OF_WEEK);
        int _lastDay = _cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        int _year = _cal.get(Calendar.YEAR);
        int _month = _cal.get(Calendar.MONTH)+1;
        int _dayOfMonth = 2 - _firstDayOfWeek;

        Calendar _calToday = Calendar.getInstance(Locale.JAPANESE);
        int _yearToday = _calToday.get(Calendar.YEAR);
        int _monthToday = _calToday.get(Calendar.MONTH)+1;
        int _dayOfMonthToday = _calToday.get(Calendar.DAY_OF_MONTH);

        _sb.append(
            "<table>"
                +"<caption>"
                    +_year+"年<strong>"+_month+"</strong>月"
                +"</caption>"
                +"<tbody>"
                    +"<tr><th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th></tr>"
        );
        for (int _row = 0; _row < 6; _row++) {
            _sb.append("<tr>");
            for (int _col = 0; _col < 7; _col++) {
                if(1 <= _dayOfMonth && _dayOfMonth <= _lastDay){
                    boolean _isToday = _year==_yearToday && _month==_monthToday && _dayOfMonth==_dayOfMonthToday;
                    _cal.set(Calendar.MONTH, _month-1);
                    _cal.set(Calendar.DAY_OF_MONTH, _dayOfMonth);
                    boolean _isPastDay = _cal.before(_calToday);
                    boolean _isCloseDay = _col==0;
                    String _timeline = "";
                    String _comment = "";
                    String _holiday = "";
                    switch(_col){
                        case 0: _timeline = "<span class=\"close\">休業日</span>"; break;
                        case 4: _timeline = "<span>17:00-21:00</span>"; break;
                        case 6: _timeline = "<span>15:00-21:00</span>"; break;
                        default: _timeline = "<span>15:00-22:00</span>"; break;
                    }
                    db.PublicHolidays _holidayCal = getHoliday(dbPublicHolidays, _year+"/"+String.format("%02d",_month)+"/"+String.format("%02d",_dayOfMonth));
                    if(_holidayCal!=null){
                        _isCloseDay = true;
                        _timeline = "<span class=\"close\">休業日</span>";
                        _holiday = "<span class=\"holiday\">"+esc(_holidayCal.getComment())+"</span>";
                    }
                    db.ServiceCalendar _serviceCal = getSchedule(dbServiceCalendar, _year+"/"+String.format("%02d",_month)+"/"+String.format("%02d",_dayOfMonth));
                    if(_serviceCal!=null){
                        _isCloseDay = _serviceCal.isCloseDay();
                        if(_isCloseDay)
                            _timeline = "<span class=\"close\">休業日</span>";
                        else
                            _timeline = "<span>"+_serviceCal.getHourStart()+":00-"+_serviceCal.getHourEnd()+":00</span>";
                        _comment = "<span class=\"comment\">"+esc(_serviceCal.getComment())+"</span>";
                    }
                    _sb.append(
                            "<td"
                                +" class=\""+(_isToday?" today":"")+(_isCloseDay?" close":"")+(_isPastDay?" past":"")+"\">"
                                +"<div>"+_dayOfMonth+"</div>"
                                +_holiday
                                +_timeline
                                +_comment
                                +"</td>"
                    );
                }else{
                    _sb.append("<td></td>");
                }
                _dayOfMonth++;
            }//for(_col)
            _sb.append("</tr>");
            if(_lastDay<_dayOfMonth)
                break;
        }//for(_row)
        _sb.append(
            "</tbody>"
        +"</table>"
        );
        return new String(_sb);
    }

    /**
     * 該当日のデータを取得
     */
    private static db.ServiceCalendar getSchedule(ArrayList<db.ServiceCalendar> dbServiceCalendar, String _date){
        for(db.ServiceCalendar _serviceCal : dbServiceCalendar)
            if(_serviceCal.getDate().equals(_date))
                return _serviceCal;
        return null;
    }
    private static db.PublicHolidays getHoliday(ArrayList<db.PublicHolidays> dbPublicHolidays, String _date){
        for(db.PublicHolidays _holidayCal : dbPublicHolidays)
            if(_holidayCal.getDate().equals(_date))
                return _holidayCal;
        return null;
    }

    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>