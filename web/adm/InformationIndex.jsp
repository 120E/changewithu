<%@page import="constant.Values"%>
<%@page import="util.StringEscapeUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>管理画面</title>
        <link rel="stylesheet" media="all" href="../css/adm.css?=20200603">
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css"/>
	<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script>
            $(function() {
                // 指定したテキストボックスにカレンダー表示
                $(".datepicker").datepicker();
                
                // 日本語化
                $.datepicker.regional['ja'] = {
                  closeText: '閉じる',
                  prevText: '<前',
                  nextText: '次>',
                  currentText: '今日',
                  monthNames: ['1月','2月','3月','4月','5月','6月',
                  '7月','8月','9月','10月','11月','12月'],
                  monthNamesShort: ['1月','2月','3月','4月','5月','6月',
                  '7月','8月','9月','10月','11月','12月'],
                  dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
                  dayNamesShort: ['日','月','火','水','木','金','土'],
                  dayNamesMin: ['日','月','火','水','木','金','土'],
                  weekHeader: '週',
                  dateFormat: 'yy/m/d',
                  firstDay: 0,
                  isRTL: false,
                  showMonthAfterYear: true,
                  yearSuffix: '年'};
                $.datepicker.setDefaults($.datepicker.regional['ja']);
            });
        </script>
    </head>
    <body><%
        req.AdmInformation _req = (req.AdmInformation)request.getAttribute("req");
        %>
        <a class="logo" href="./"><img src="../images/change_logo.png"></a>
        <div class="breadcrumb">
            <a href="./">管理画面トップ</a>
            &gt;
            おしらせ管理（一覧）
        </div>

        <h1>おしらせ管理</h1>
        <form class="add" action="information_edit.html" method="post">
            <h3>▼追加登録はこちら</h3>
            <div class="error_message">入力エラー：日付、タイトルは必須入力です。</div>
            <dl>
                <dt>表示／非表示<span class="red">必須選択</span></dt>
                <dd>
                    <select name="state">
                        <option value="1" <%=_req.getState()==1?"selected":""%>>表示する</option>
                        <option value="0" <%=_req.getState()==0?"selected":""%>>表示しない</option>
                    </select>
                </dd>
                <dt>日付<span class="red">必須入力</span></dt>
                <dd><input name="disp_date" type="text" class="datepicker" value="<%=esc(_req.getDispDate())%>" readonly></dd>
                <dt>タイトル<span class="red">必須入力</span></dt>
                <dd><input name="title" type="text" value="<%=esc(_req.getTitle())%>" placeholder="おしらせタイトルを入力します"></dd>
                <dt>本文</dt>
                <dd><textarea name="content" rows="10" placeholder="おしらせの本文を入力します（空でもOK）"><%=esc(_req.getContent())%></textarea></dd>
            </dl>
            <input type="reset" value="リセット">
            <input type="submit" value="　登録　">
            <input type="hidden" name="action" value="add">
            <input type="hidden" name="step" value="<%=Values.STEP_AFFECT%>">
            <input type="hidden" name="id" value="<%=_req.getId()%>">
        </form>
        <script>
            $(function(){
                $('input[type=submit]').on('click', function() {
                    if($('input[name=disp_date]').val().length==0
                    || $('input[name=title]').val().length==0){
                        $('.error_message').show();
                        return false;
                    }
                });
            });
        </script>
        
        <div id="list" class="list">
            <h3>▼登録済のおしらせ</h3><%
            ArrayList<db.Information> dbInformation = (ArrayList<db.Information>)request.getAttribute("dbInformation");
            if(dbInformation==null || dbInformation.isEmpty()){ %>
                現在、おしらせはありません<%
            }else{ %>
                <dl><%
                    for(db.Information _info : dbInformation){ %>
                        <dt class="<%=_info.getState()==0?"invisible":""%>">
                            <a class="btn edit" href="information_edit.html?action=edit&id=<%=_info.getId()%>"></a>
                            <a class="btn trash" href="information_edit.html?action=del&id=<%=_info.getId()%>"></a>
                            <span class="date"><%=esc(_info.getDispDate())%></span>
                            <span class="title"><%=esc(_info.getTitle())%></span>
                        </dt><%
                        if(0<_info.getContent().length()){ %>
                            <dd>
                                <%=escBR(_info.getContent())%>
                            </dd><%
                        }//if
                    }//for
                    %>
                </dl><%
            }//else
            %>
        </div>
    </body>
</html>
<%!
    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>