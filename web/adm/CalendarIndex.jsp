<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Calendar"%>
<%@page import="constant.Values"%>
<%@page import="util.StringEscapeUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>管理画面</title>
        <link rel="stylesheet" media="all" href="../css/adm.css">
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css"/>
	<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script>
            $(function() {
                // 指定したテキストボックスにカレンダー表示
                $(".datepicker").datepicker();
                
                // 日本語化
                $.datepicker.regional['ja'] = {
                  closeText: '閉じる',
                  prevText: '<前',
                  nextText: '次>',
                  currentText: '今日',
                  monthNames: ['1月','2月','3月','4月','5月','6月',
                  '7月','8月','9月','10月','11月','12月'],
                  monthNamesShort: ['1月','2月','3月','4月','5月','6月',
                  '7月','8月','9月','10月','11月','12月'],
                  dayNames: ['日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日'],
                  dayNamesShort: ['日','月','火','水','木','金','土'],
                  dayNamesMin: ['日','月','火','水','木','金','土'],
                  weekHeader: '週',
                  dateFormat: 'yy/m/d',
                  firstDay: 0,
                  isRTL: false,
                  showMonthAfterYear: true,
                  yearSuffix: '年'};
                $.datepicker.setDefaults($.datepicker.regional['ja']);
            });
        </script>
    </head>
    <body>
        <a class="logo" href="./"><img src="../images/logo_w.png"></a>
        <div class="breadcrumb">
            <a href="./">管理画面トップ</a>
            &gt;
            カレンダー管理
        </div>

        <h1>カレンダー管理</h1>
        <div id="calendar" class="calendar">
            <div class="customCalendar"><%
                ArrayList<db.ServiceCalendar> dbServiceCalendarCurrent = (ArrayList<db.ServiceCalendar>)request.getAttribute("dbServiceCalendarCurrent");
                ArrayList<db.ServiceCalendar> dbServiceCalendarNext = (ArrayList<db.ServiceCalendar>)request.getAttribute("dbServiceCalendarNext");
                ArrayList<db.PublicHolidays> dbPublicHolidaysCurrent = (ArrayList<db.PublicHolidays>)request.getAttribute("dbPublicHolidaysCurrent");
                ArrayList<db.PublicHolidays> dbPublicHolidaysNext = (ArrayList<db.PublicHolidays>)request.getAttribute("dbPublicHolidaysNext");
                //カレンダー2ヶ月表示
                //getCalendarで1月分を構成
                try{
                    Calendar _cal = Calendar.getInstance(Locale.JAPANESE);
                    out.println(getCalendar(dbServiceCalendarCurrent, dbPublicHolidaysCurrent, _cal));
                    _cal.set(Calendar.MONTH, _cal.get(Calendar.MONTH)+1);
                    out.println(getCalendar(dbServiceCalendarNext, dbPublicHolidaysNext, _cal));
                }catch(Exception e){
                    e.printStackTrace(System.out);
                }
                %>
            </div>
        </div>
        <script>
            $(function(){
                $('td.present').on('click',function(){
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.date').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.weekday').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.holiday').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.hour_start').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.hour_end').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.is_close_day').text());
//                    console.log('OOOOOOOOOOOOKKKKKKKKKKKKK'+$(this).find('.comment').text());
                    $('input[name=date]').val($(this).find('.date').text());
                    if($(this).find('.is_close_day').text()=='1'){
                        $('input[name=is_close_day]:eq(1)').prop('checked', true);
                    }else{
                        $('input[name=is_close_day]:eq(0)').prop('checked', true);
                    }
                    $('select[name=hour_start]').val($(this).find('.hour_start').text());
                    $('select[name=hour_end]').val($(this).find('.hour_end').text());
                    $('input[name=comment]').val($(this).find('.comment').text());
                    var _weekday = "";
                    if($(this).find('.weekday').text()==0){_weekday='（日）';}
                    else if($(this).find('.weekday').text()==1){_weekday='（月）';}
                    else if($(this).find('.weekday').text()==2){_weekday='（火）';}
                    else if($(this).find('.weekday').text()==3){_weekday='（水）';}
                    else if($(this).find('.weekday').text()==4){_weekday='（木）';}
                    else if($(this).find('.weekday').text()==5){_weekday='（金）';}
                    else if($(this).find('.weekday').text()==6){_weekday='（土）';}
                    $('.edit_schedule .date').text($(this).find('.date').text()+_weekday);
                    $('.edit_schedule .holiday').text($(this).find('.holiday').text());
                    //ページ最下へ移動
                    $('form.edit_schedule').show();
                    var element = document.documentElement;
                    var bottom = element.scrollHeight - element.clientHeight;
                    window.scroll(0, bottom);
                });
            });
        </script>

        <form class="edit_schedule" action="calendar_edit.html" method="post">
            <h3>▼登録はこちら</h3>
            <div class="error_message">入力エラー：日付、タイトルは必須入力です。</div>
            <ul>
                <li>
                    日付：<input name="date" type="hidden" value="">
                    <span class="date"></span>
                    <span class="holiday"></span>
                </li>
                <li>
                    <input name="is_close_day" type="radio" value="0"> 営業日
                    &nbsp;
                    <input name="is_close_day" type="radio" value="1"> 休業日
                </li>
                <li>
                    営業時間帯：
                    <select name="hour_start">
                        <option value="0"></option><%
                        for(int n=13; n<=20; n++){ %>
                            <option value="<%=n%>"><%=n%></option><%
                        } %>
                    </select>時
                    ～
                    <select name="hour_end">
                        <option value="0"></option><%
                        for(int n=15; n<=24; n++){ %>
                            <option value="<%=n%>"><%=n%></option><%
                        } %>
                    </select>時<br>
                    <span class="red">※「休業日」を選択した場合は、時間帯は無視されます。</span>
                </li>
                <li>
                    <input name="comment" type="text" placeholder="コメントを入力します（空でもOK）" value="">
                </li>
            </ul>
            <input type="reset" value="キャンセル" onclick="$('form.edit_schedule').hide();">
            <input type="submit" value="　更　新　">
            <input type="hidden" name="action" value="edit">
            <input type="hidden" name="step" value="<%=Values.STEP_AFFECT%>">
        </form>
        <script>
            $(function(){
                $('input[type=submit]').on('click', function() {
                    if($('input[name=disp_date]').val().length==0
                    || $('input[name=title]').val().length==0){
                        $('.error_message').show();
                        return false;
                    }
                });
            });
        </script>

    </body>
</html>
<%!
    /**
     * カレンダー表示部
     */
    private String getCalendar(ArrayList<db.ServiceCalendar> dbServiceCalendar, ArrayList<db.PublicHolidays> dbPublicHolidays, Calendar _cal){

        StringBuffer _sb = new StringBuffer();
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy/MM/dd");
        _cal.set(Calendar.DAY_OF_MONTH, 1);
        int _firstDayOfWeek = _cal.get(Calendar.DAY_OF_WEEK);
        int _lastDay = _cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        int _year = _cal.get(Calendar.YEAR);
        int _month = _cal.get(Calendar.MONTH)+1;
        int _dayOfMonth = 2 - _firstDayOfWeek;

        Calendar _calToday = Calendar.getInstance(Locale.JAPANESE);
        int _yearToday = _calToday.get(Calendar.YEAR);
        int _monthToday = _calToday.get(Calendar.MONTH)+1;
        int _dayOfMonthToday = _calToday.get(Calendar.DAY_OF_MONTH);

        _sb.append(
            "<table>"
                +"<caption>"
                    +_year+"年<strong>"+_month+"</strong>月"
                +"</caption>"
                +"<tbody>"
                    +"<tr><th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th></tr>"
        );
        for (int _row = 0; _row < 6; _row++) {
            _sb.append("<tr>");
            for (int _col = 0; _col < 7; _col++) {
                if(1 <= _dayOfMonth && _dayOfMonth <= _lastDay){
                    boolean _isToday = _year==_yearToday && _month==_monthToday && _dayOfMonth==_dayOfMonthToday;
                    _cal.set(Calendar.MONTH, _month-1);
                    _cal.set(Calendar.DAY_OF_MONTH, _dayOfMonth);
                    boolean _isPastDay = _cal.before(_calToday);
                    boolean _isCloseDay = _col==0;
                    String _timeline = "";
                    String _comment = "";
                    String _holiday = "";
                    int _hourStart = 0;
                    int _hourEnd = 0;
                    switch(_col){
                        case 0: _timeline = "<span class=\"close\">休業日</span>"; break;
                        case 4: _timeline = "<span>17:00-21:00</span>"; _hourStart=17; _hourEnd=21; break;
                        case 6: _timeline = "<span>15:00-21:00</span>"; _hourStart=15; _hourEnd=21; break;
                        default: _timeline = "<span>15:00-22:00</span>"; _hourStart=15; _hourEnd=22; break;
                    }
                    db.PublicHolidays _holidayCal = getHoliday(dbPublicHolidays, _year+"/"+String.format("%02d",_month)+"/"+String.format("%02d",_dayOfMonth));
                    if(_holidayCal!=null){
                        _isCloseDay = true;
                        _timeline = "<span class=\"close\">休業日</span>";
                        _holiday = "<span class=\"holiday\">"+esc(_holidayCal.getComment())+"</span>";
                    }
                    db.ServiceCalendar _serviceCal = getSchedule(dbServiceCalendar, _year+"/"+String.format("%02d",_month)+"/"+String.format("%02d",_dayOfMonth));
                    if(_serviceCal!=null){
                        _isCloseDay = _serviceCal.isCloseDay();
                        if(_isCloseDay){
                            _timeline = "<span class=\"close\">休業日</span>";
                        }else{
                            _hourStart = _serviceCal.getHourStart();
                            _hourEnd = _serviceCal.getHourEnd();
                            _timeline = "<span>"+_hourStart+":00-"+_hourEnd+":00</span>";
                        }
                        _comment = "<span class=\"comment\">"+esc(_serviceCal.getComment())+"</span>";
                    }
                    _sb.append(
                            "<td"
                                +" class=\""+(_isToday?" today":"")+(_isCloseDay?" close":"")+(_isPastDay?" past":" present")+"\">"
                                +"<div>"+_dayOfMonth+"</div>"
                                +_holiday
                                +_timeline
                                +_comment
                                +"<span class=\"hidden date\">"+_year+"/"+_month+"/"+_dayOfMonth+"</span>"
                                +"<span class=\"hidden weekday\">"+_col+"</span>"
                                +"<span class=\"hidden hour_start\">"+_hourStart+"</span>"
                                +"<span class=\"hidden hour_end\">"+_hourEnd+"</span>"
                                +"<span class=\"hidden is_close_day\">"+(_isCloseDay?1:0)+"</span>"
                                +"</td>"
                    );
                }else{
                    _sb.append("<td></td>");
                }
                _dayOfMonth++;
            }//for(_col)
            _sb.append("</tr>");
            if(_lastDay<_dayOfMonth)
                break;
        }//for(_row)
        _sb.append(
            "</tbody>"
        +"</table>"
        );
        return new String(_sb);
    }

    /**
     * 該当日のデータを取得
     */
    private static db.ServiceCalendar getSchedule(ArrayList<db.ServiceCalendar> dbServiceCalendar, String _date){
        for(db.ServiceCalendar _serviceCal : dbServiceCalendar)
            if(_serviceCal.getDate().equals(_date))
                return _serviceCal;
        return null;
    }
    private static db.PublicHolidays getHoliday(ArrayList<db.PublicHolidays> dbPublicHolidays, String _date){
        for(db.PublicHolidays _holidayCal : dbPublicHolidays)
            if(_holidayCal.getDate().equals(_date))
                return _holidayCal;
        return null;
    }

    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>