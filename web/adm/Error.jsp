<%@page import="org.apache.commons.lang.BooleanUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>管理画面</title>
        <link rel="stylesheet" media="all" href="../css/adm.css">
    </head>
    <body>
        <a class="logo" href="./"><img src="../images/logo_w.png"></a>
        <h1>HP管理</h1>
        <p style="margin:100px auto;"><%
            boolean result = BooleanUtils.toBoolean((String)request.getAttribute("result"));
            String nextPage = (String)request.getAttribute("next_page");
            String validMessage = "処理は正常に終了しました。";
            String invalidMessage = "エラーにより、処理は終了しませんでした。<br>管理会社へご連絡ください。";
            if(result){
                %><%=validMessage%><%
            }else{
                %><font style="color:#f00;"><%=invalidMessage%></font><%
            }
            %>
        </p>
        <a href="<%=nextPage%>" class="btn">OK</a>
    </body>
</html>
