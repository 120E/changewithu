/*
 * ハンバーガーメニュー
 */
$(function() {
    $('header .hamburger').on('click', function() {
        $(this).toggleClass('active');
        if($('header .menu').css('display') === 'block') {
            $('header .menu').slideUp('1500');
            $('.menu_mask').remove();
        }else {
            $('header').after("<div onclick=\"javascript:void(0);\" class=\"menu_mask\"></div>");
            $('header .menu').slideDown('1500');
        }
    });
    $('header .menu a').on('click', function() {
        closeMenu();
    });
});
function closeMenu(){
    if($('header .hamburger').css('display') != 'none') {
        if($('header .menu').css('display') === 'block') {
            $('header .hamburger').removeClass('active');
            $('header .menu').slideUp('1500');
            $('.menu_mask').remove();
        }
    }
}


/*
 * トップページおしらせ、スクロール時フェイドアウト（スマホのみ）
 */
$(function() {
    $(this).scroll(function () {
        if($(document).innerWidth() < 768){
            if ($(this).scrollTop() > 50) {
                $('.info').fadeOut();
            } else {
                $('.info').fadeIn();
            }
        }
        if ($(this).scrollTop() > 500) {
            $('.goTop').fadeIn();
        } else {
            $('.goTop').fadeOut();
        }
    });
    $('.goTop').fadeOut();
});


/*
 * スムーズスクロール
 */
$(function(){
    $('a[href^="#"]').click(function(){
        var adjust = -50;
        var speed = 400;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top + adjust;
        $('body,html').animate({scrollTop:position}, speed, 'swing');
        return false;
    });
});
