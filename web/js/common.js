


/*
 * トップページおしらせ、スクロール時フェイドアウト（スマホのみ）
 */
$(function() {
    $(this).scroll(function () {
        if($(document).innerWidth() < 768){
            if ($(this).scrollTop() > 50) {
                $('.info').fadeOut();
            } else {
                $('.info').fadeIn();
            }
        }
        if ($(this).scrollTop() > 500) {
            $('.page_top').fadeIn();
        } else {
            $('.page_top').fadeOut();
        }
    });
    $('.page_top').fadeOut();
});


/*
 * スムーズスクロール
 */
$(function(){
    $('a[href^="#"]').click(function(){
        var adjust = -50;
        var speed = 400;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top + adjust;

//        モーダルウィンドウはスクロールしない
        if($(this).attr('class') ==='confirmation_open' || 
            $(this).attr('class') ==='confirmation-close'){
        }else{
            $('body,html').animate({scrollTop:position}, speed, 'swing');
        }
        return false;
    });
});
/*
 * 本人確認表示
 */
$(function(){
    $('.confirmation_open').on('click',function(){
        $('.js-modal').fadeIn();
        return false;
    });
    $('.confirmation-close').on('click',function(){
        $('.js-modal').fadeOut();
        return false;
    });
});
