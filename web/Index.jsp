<%@page import="java.util.Locale"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.ArrayList"%>
<%@page import="util.StringEscapeUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KRHCSM8');</script>
        <!-- End Google Tag Manager -->
        <title>結婚相談所なら | チェンジをともに</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta name="description" content="「チェンジをともに（チェンとも）」は理想の出会いをカタチにする結婚相談所です。あなたのご結婚までトレーナー・カウンセラーが全力でサポートいたします。">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <link rel="stylesheet" media="all" href="css/common.css">
        <link rel="stylesheet" media="screen and (max-width: 767px)" href="css/common_sp.css">
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRHCSM8"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="base_contents">
            <header>
                <div class="change_logo">
                    <a href="#"><img src="images/change_logo.png" alt=""/></a>
                </div>
                <div class="right_menu">
                    <a class="right_menu_block menu_consultation" target="blank"  href="https://forms.gle/mS2hvpg3a72Vu4oq6">無料相談予約</a>
                </div>
                <nav class="menu">
                    <ul>
                        <li><a href="#prices">コース・料金</a></li>
                        <li><a href="#safety">安心/安全な出会い</a></li>
                        <li><a href="#activity">活動開始の流れ</a></li>
                        <li><a href="policy.html">サイトポリシー</a></li>
                    </ul>
                </nav>
            </header>
            <div class="main_contents">
                
                <div class="key_visual">
                    <section class="info">
                        <span class="label">
                            おしらせ
                        </span>
                        <a href="information.html" class="article"><%
                            ArrayList<db.Information> dbInformation = (ArrayList<db.Information>)request.getAttribute("dbInformation");
                            if(dbInformation==null || dbInformation.isEmpty()){ %>
                                現在、おしらせはありません<%
                            }else{ %>
                                <%=esc(dbInformation.get(0).getTitle())%><%
                                if(dbInformation.get(0).isNew()) { %>
                                    <span>new</span><%
                                }
                            } %>
                        </a>
                    </section>
                    <div class="visual_title title_pc"><img src="images/key_visual_title.png" alt=""/></div>
                    <div class="visual_title title_sp"><img src="images/key_visual_title_sp.png" alt=""/></div>
                    <div class="visual_banner"><img src="images/key_visual_banner.png" alt=""/></div>
                    <div class="visual_point"><img src="images/key_visual_point.png" alt=""/></div>
                </div>
                <div class="block_contents">
                    <div class="encounter">
                        <div class="encounter_content">
                            <div class="encounter_title">
                                <img src="images/title_encounter.png" alt="出会いの保証制度"/>
                            </div>
                            <div class="encounter_block">
                                <div class="encounter_text">
                                <p>3カ月以内に出会いがなければ登録料を全額返金いたします。</p>
                                <p class="encounter_date">（期限2020年12月末）</p>
                                    <p>詳しくはメールかお電話でお問い合わせください。</p>
                                <table class="content_address">
                                    <tr>
                                        <td>Tel</td>
                                        <td>：</td>
                                        <td><a class="tel" href="tel:080-6285-6285">080-6285-6285</a></td>
                                    </tr>
                                    <tr>
                                        <td>Mail</td>
                                        <td>：</td>
                                        <td><a class="mail" href="mailto:change.with.u.kochi@gmail.com">change.with.u.kochi@gmail.com</a></td>
                                    </tr>
                                </table>
                                </div>
                                <div class="encounter_img">
                                    <img src="images/encounter.png" alt=""/>
                                </div>
                            </div>
                            <div class="encounter_page">
                                <img src="images/encounter_page.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="prices" class="block_contents back_color">
                    <div class="content_title">
                        <h1>コース・料金</h1>
                    </div>
                    <div class="content_text">
                        <p><span class="red ">★高知最安値★</span><span class="content_br"></span>　入会金・登録料無料で始められるコースがここにはある！！</p>
                        <p>さらに　【入会特典】　<span class="red">30日間無料！！</span></p>
                        <p>（価格はすべて税込み表記です。）</p>
                    </div>
                    <div class="content_course_table">
                        <table class="course">
                            <tr>
                                <th>
                                    
                                </th>
                                <th>
                                    キャンペーン価格
                                </th>
<!--                                <th>
                                    通常コース
                                </th>-->
                                <th>
                                    定額コース
                                </th>
                                <th>
                                    美健酵素コース
                                </th>
                            </tr>
                            <tr>
                                <td>
                                  計 
                                </td>
                                <td>
                                    <span>&yen;</span>27,500
                                </td>
<!--                                <td>
                                    <span>&yen;</span>62,700
                                </td>-->
                                <td>
                                    <span>&yen;</span>9,900
                                </td>
                                <td>
                                    <span>&yen;</span>15,400
                                </td>
                            </tr>
                            <tr>
                                <td>入会金</td>
                                <td><span>&yen;</span>22,000</td>
<!--                                <td><span>&yen;</span>22,000</td>-->
                                <td><span>&yen;</span>0</td>
                                <td><span>&yen;</span>0</td>
                            </tr>
                            <tr>
                                <td>登録料</td>
                                <td><span>&yen;</span>0</td>
<!--                                <td><span>&yen;</span>33,000</td>-->
                                <td><span>&yen;</span>0</td>
                                <td><span>&yen;</span>0</td>
                            </tr>
                            <tr>
                                <td>月会費</td>
                                <td><span>&yen;</span>5,500</td>
<!--                                <td><span>&yen;</span>7,700</td>-->
                                <td><span>&yen;</span>9,900</td>
                                <td><span>&yen;</span>15,400</td>
                            </tr>
                            <tr>
                                <td>見合い申請回数</td>
                                <td>3回/月</td>
<!--                                <td>5回/月</td>-->
                                <td>10回/月</td>
                                <td>10回/月</td>
                            </tr>
                            <tr>
                                <td>見合い料</td>
                                <td colspan="3"><span>&yen;</span>5,500</td>
                            </tr>
                            <tr>
                                <td>成婚料</td>
                                <td colspan="3"><span>&yen;</span>275,000</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><div class="toggle_under"></div></td>
<!--                                <td><div class="toggle_under"></div></td>-->
                                <td><div class="toggle_under"></div></td>
                                <td><div class="toggle_under"></div></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><a class="course_detail" href="course.html#campaign">コースの詳細へ</a></td>
<!--                                <td><a class="course_detail" href="course.html#normally">コースの詳細へ</a></td>-->
                                <td><a class="course_detail" href="course.html#flat_rate">コースの詳細へ</a></td>
                                <td><a class="course_detail" href="course.html#beauty">コースの詳細へ</a></td>
                            </tr>
                        </table>
                    </div>
                    <div class="content_course_sp">
                        <div class="course_block">
                            <div class="course_title">▶ キャンペーン価格</div>
                            <table class="price_table">
                                <tr>
                                    <th>入会金</th>
                                    <th>登録料</th>
                                    <th>月会費</th>
                                    <th>計</th>
                                </tr>
                                <tr>
                                    <td><span>&yen;</span>22,000</td>
                                    <td><span>&yen;</span>0</td>
                                    <td><span>&yen;</span>5,500</td>
                                    <td><span>&yen;</span>27,500</td>
                                </tr>
                            </table>
                            <ul class="price_menu">
                                <li>見合い申請回数：3回/月</li>
                                <li>見合い料：<span>&yen;</span>5,500</li>
                                <li>成婚料　：<span>&yen;</span>275,000</li>
                            </ul>
                            <a class="course_detail" href="course.html#campaign">コースの詳細へ</a>
                        </div>
                        <div class="content_break"></div>
<!--                        <div class="course_block">
                            <div class="course_title">▶ 通常コース</div>
                            <table class="price_table">
                                <tr>
                                    <th>入会金</th>
                                    <th>登録料</th>
                                    <th>月会費</th>
                                    <th>計</th>
                                </tr>
                                <tr>
                                    <td><span>&yen;</span>22,000</td>
                                    <td><span>&yen;</span>33,000</td>
                                    <td><span>&yen;</span>7,700</td>
                                    <td><span>&yen;</span>62,700</td>
                                </tr>
                            </table>
                            <ul class="price_menu">
                                <li>見合い申請回数：5回/月</li>
                                <li>見合い料：<span>&yen;</span>5,500</li>
                                <li>成婚料　：<span>&yen;</span>275,000</li>
                            </ul>
                            <a class="course_detail" href="course.html#normally">コースの詳細へ</a>
                        </div>
                        <div class="content_break"></div>-->
                        <div class="course_block">
                            <div class="course_title">▶ 定額コース</div>
                            <table class="price_table">
                                <tr>
                                    <th>入会金</th>
                                    <th>登録料</th>
                                    <th>月会費</th>
                                    <th>計</th>
                                </tr>
                                <tr>
                                    <td><span>&yen;</span>0</td>
                                    <td><span>&yen;</span>0</td>
                                    <td><span>&yen;</span>9,900</td>
                                    <td><span>&yen;</span>9,900</td>
                                </tr>
                            </table>
                            <ul class="price_menu">
                                <li>見合い申請回数：10回/月</li>
                                <li>見合い料：<span>&yen;</span>5,500</li>
                                <li>成婚料　：<span>&yen;</span>275,000</li>
                            </ul>
                            <a class="course_detail" href="course.html#flat_rate">コースの詳細へ</a>
                        </div>
                        <div class="content_break"></div>
                        <div class="course_block">
                            <div class="course_title">▶ 美健酵素コース</div>
                            <table class="price_table">
                                <tr>
                                    <th>入会金</th>
                                    <th>登録料</th>
                                    <th>月会費</th>
                                    <th>計</th>
                                </tr>
                                <tr>
                                    <td><span>&yen;</span>0</td>
                                    <td><span>&yen;</span>0</td>
                                    <td><span>&yen;</span>15,400</td>
                                    <td><span>&yen;</span>15,400</td>
                                </tr>
                            </table>
                            <ul class="price_menu">
                                <li>見合い申請回数：10回/月</li>
                                <li>見合い料：<span>&yen;</span>5,500</li>
                                <li>成婚料　：<span>&yen;</span>275,000</li>
                            </ul>
                            <a class="course_detail" href="course.html#beauty">コースの詳細へ</a>
                        </div>
                    </div>
                </div>
                <div id="safety" class="block_contents">
                    <div class="content_title">
                        <h1>安心/安全な出会い</h1>
                    </div>
                    <div class="content_text text_long">
                        <p>“チェンジをともに” は株式会社日本ブライダル連盟（通称：BIU）に加盟しており2020年2月現在、BIUでは50,650人の会員が登録・利用をしています。</p>
                    </div>
                    <div class="safety_block">
                        <div class="safety_text">
                            <h3>BIU登録会員の年齢<span class="safety_date">（2015年8月時点）</span></h3>
                                        <p>昨今の晩婚化の影響もあって40代前半の会員が多くなっております。<br/>
                                            そのため、互いに信頼し合ってお相手を見つけられる方が多いのが特徴です。</p>
                        </div>
                        <div class="safety_img">
                            <img src="images/safety_bar_graph.png" alt=""/>
                        </div>
                    </div>
                    <div class="content_break"></div>
                    <div class="safety_block pie_chart">
                        <div class="safety_text">
                            <h3>BIU登録会員の年齢<span class="safety_date">（2015年8月時点）</span></h3>
                            <p>男女共に半数以上の学歴が短大卒以上となっています。<br/>
                                高校、専門卒の方もバランスよく在籍されています。</p>
                        </div>
                        <div class="safety_img">
                            <img src="images/safety_pie_chart.png" alt=""/>
                        </div>
                    </div>
                </div>
                <div id="activity" class="block_contents back_color">
                    <div class="content_title">
                        <h1>活動開始の流れ</h1>
                    </div>
                    <div class="content_text text_long">
                        <p>全てのお客様に本人確認・収入証明の確認書類を提出していただいておりますので、信頼できるお相手をお探しいただけます。</p>
                    </div>
                    <div class="activity_block">
                        <div class="activity_flow flow_01">
                            <div class="flow_box">
                                <div class="flow_img">
                                    <img src="images/flow_01.png" alt=""/>
                                </div>
                                <h2><span class="flow_number">01.</span>登録受け付け</h2>
                                <p>電話かメールでのご連絡をお待ちしております。</p>
                                <table class="content_address">
                                    <tr>
                                        <td>Tel</td>
                                        <td>：</td>
                                        <td><a class="tel" href="tel:080-6285-6285">080-6285-6285</a></td>
                                    </tr>
                                    <tr>
                                        <td>Mail</td>
                                        <td>：</td>
                                        <td><a class="mail" href="mailto:change.with.u.kochi@gmail.com">change.with.u.kochi@gmail.com</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="activity_flow flow_02">
                            <div class="flow_box">
                                <div class="flow_img">
                                    <img src="images/flow_02.png" alt=""/>
                                </div>
                                <h2><span class="flow_number">02.</span>本人確認等</h2>
                                <p>本人確認と収入証明のための書類を提出していただきます。</p>
                                <p>確認書類について、詳しくは<a class="confirmation_open" href="#">コチラ</a>をご確認ください。</p>
                            </div>
                        </div>
                        <div class="activity_flow flow_03">
                            <div class="flow_box">
                                <div class="flow_img">
                                    <img src="images/flow_03.png" alt=""/>
                                </div>
                                <h2><span class="flow_number">03.</span>システムID送付</h2>
                                <p>”チェンジをともに” が加盟している○○○のシステムIDをメールにてお送りします。</p>
                            </div>
                        </div>
                        <div class="activity_flow flow_04">
                            <div class="flow_box">
                                <div class="flow_img">
                                    <img src="images/flow_04.png" alt=""/>
                                </div>
                                <h2><span class="flow_number">04.</span>活動開始</h2>
                                <p>いよいよ活動開始です。気になるお相手のご紹介や、会員様のご相談に親身に対応いたします。</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="site_footer">
                <div class="page_top">
                    <a href="#"><img src="images/page_top.png" alt=""/></a>
                </div>
                <h2>チェンジをともに</h2>
                <div class="footer_">
                    <div class="footer_address">
                        <p>TEL：<a class="tel" href="tel:080-6285-6285">080-6285-6285</a></p>
                        <p>MAIL：<a class="mail" href="mailto:change.with.u.kochi@gmail.com">change.with.u.kochi@gmail.com</a></p>
                        <p class="footer_inquiry">ADDRESS：高知市帯屋町2丁目1-14リエイゾン4F<br />※お問い合わせは電話かメールにてお願いいたします。</p>
                    </div>
                    <nav class="footer_navi">
                        <ul>
                            <li><a href="policy.html">サイトポリシー</a></li>
                            <li><a target="blank" href="https://www.biu.jp/">日本ブライダル連盟</a></li>
                            <li><a href="#">Facebook</a></li>
                        </ul>
                    </nav>
                </div>
            </footer>
            <div class="copyright">
                Copyright ©  2020 チェンジをともに All Rights Reserved.
            </div> 
        </div>
        <div class="modal js-modal">
            <div class="modal__bg confirmation-close"></div>
            <div class="modal__content">
                <a class="confirmation-close" href="#">
                    <div class="close_button"></div>
                </a>
                <h3>本人確認で提出いただける書類</h3>
                <ul>
                    <li><p>運転免許証</p></li>
                    <li><p>日本国パスポート</p>
                        <p class="attention">
                            ※日本国政府発行の顔写真、氏名、生年月日、現住所が記載で有効期限内のもの。
                            確認書類の現住所と異なる場合、または、現住所記載欄がない場合は、住民票やご本人さま宛の現住所記載の公共料金領収書などが必要です（発行日から3ヵ月以内のもの）。
                        </p>
                    </li>
                    <li><p>在留カード（旧 外国人登録証明書）＋外国パスポート</p></li>
                    <li>
                        <p>個人番号カード（マイナンバーカード）</p>
                        <p class="attention">※通知カードは本人確認書類としてご使用できません。</p>
                    </li>
                </ul>
                <br />
                <br />
                <h3>本人確認で提出いただける書類</h3>
                <ul>
                    <li><p>所得証明書／源泉徴収票</p></li>
                    <li><p>市民税・県民税課税証明書／町県民税課税証明</p></li>
                    <li><p>給与明細書</p></li>
                    <li><p>確定申告書</p></li>
                </ul>
            </div>
        </div>
    </body>
</html>
<%!
    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>