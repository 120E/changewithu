<%@page import="util.StringEscapeUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-WTNL6SC');</script>
        <!-- End Google Tag Manager -->
        <title>おしらせ | 天河ボクシングジム</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <link rel="stylesheet" media="all" href="css/common.css?=20200601d">
        <link rel="stylesheet" media="screen and (max-width: 767px)" href="css/common_sp.css?=20200601d">
        <style>
        </style>
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WTNL6SC"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        
        <div class="base_contents">
            
            
            
            <header>
                <ul class="menu">
                    <li><a href="index.html#prices">料金</a></li>
                    <li><a href="index.html#calendar">営業カレンダー</a></li>
                    <li><a href="index.html#faq">よくあるご質問</a></li>
                    <li><a href="index.html#access">アクセス</a></li>
                    <li><a href="policy.html">サイトポリシー</a></li>
                    <li>
                       <a href="index.html"><img src="images/logo_w.png" class="logo_s"></a>
                    </li>
                </ul>
                <div class="contacts">
                    <a href="phoneto:088-855-4774"><img src="images/phone.png"></a>
                    <a href="mailto:tengaboxinggym@gmail.com"><img src="images/mail.png"></a>
                </div>
                <a href="javascript:void(0);" class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </header>
            
            
            
            
            <div class="goBack">
                <a href="index.html">&lt;&lt; もどる</a>
            </div>
            
            
            
            <div class="infoPage">
                <div class="bigtitle">おしらせ</div><%
                ArrayList<db.Information> dbInformation = (ArrayList<db.Information>)request.getAttribute("dbInformation");
                if(dbInformation==null || dbInformation.isEmpty()){ %>
                    現在、おしらせはありません<%
                }else{ %>
                    <dl><%
                        for(db.Information _info : dbInformation){ %>
                            <dt>
                                <span class="date"><%=esc(_info.getDispDate())%></span>
                                <%=esc(_info.getTitle())%>
                            </dt><%
                            if(0<_info.getContent().length()){ %>
                                <dd>
                                    <%=escBR(_info.getContent())%>
                                </dd><%
                            }//if
                        }//for
                        %>
                    </dl><%
                }//else
                %>
            </div><!-- infoPage -->
            


            <div class="copyright">
                Copyright&copy; 天河ボクシングジム　All rights reserved.
            </div>

            

            <a href="#" class="goTop"></a>

            
            
        </div><!-- base_contents -->
    </body>
</html>
<%!
    /**
     * 「StringEscapeUtils.escapeHtml」は長いので省略形
     */
    private String esc(String _value){
        return StringEscapeUtils.escapeHtml(_value);
    }
    private String escBR(String _value){
        return StringEscapeUtils.escapeHtmlPlusBr(_value);
    }
%>